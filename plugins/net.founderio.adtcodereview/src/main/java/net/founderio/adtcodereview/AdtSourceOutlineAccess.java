package net.founderio.adtcodereview;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.eclipse.core.resources.IFile;
import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.ui.texteditor.ITextEditor;

import com.sap.adt.communication.content.plaintext.IPlainTextFragmentHandler;
import com.sap.adt.communication.content.plaintext.IPlainTextPosition;
import com.sap.adt.communication.internal.testaccess.ContentHandlingFactoryForTest;
import com.sap.adt.tools.abapsource.AbapSource;
import com.sap.adt.tools.abapsource.sources.objectstructure.IObjectStructureElement;
import com.sap.adt.tools.abapsource.sources.objectstructure.ISourceObjectTypeUtility;
import com.sap.adt.tools.abapsource.sources.objectstructure.SourceObjectTypeUtility;
import com.sap.adt.tools.abapsource.ui.sources.editors.IAbapSourcePage;
import com.sap.adt.tools.abapsource.ui.sources.outline.AdtOutlineTreeContentProvider;
import com.sap.adt.tools.abapsource.ui.sources.outline.IAdtOutlineTreeNode;
import com.sap.adt.tools.core.model.atom.IAtomLink;
import com.sap.adt.tools.core.urimapping.AdtUriMappingServiceFactory;
import com.sap.adt.tools.core.urimapping.IAdtUriMappingService;

import net.founderio.adtcodereview.model.SourceRange;

/**
 * Based on
 * com.sap.adt.tools.abapsource.ui.sources.outline.AdtOutlineLinkWithEditorService
 */
@SuppressWarnings("restriction")
@Creatable
public class AdtSourceOutlineAccess {
	// TODO: This implementation only gets METHOD nodes, there are many more. e.g.
	// Outline of dictionary types, forms, data definitions,
	// public/protected/private sections, ...

	private final ISourceObjectTypeUtility sourceObjTypeUtil = new SourceObjectTypeUtility();

	public static record ObjectSelection(IObjectStructureElement element, SourceRange elementRange) {
	}

	public ObjectSelection getOutlineNodeForMethodSurroundingEditorSelection(IAbapSourcePage page) {
		if (page == null) {
			return null;
		}
		Object rootObject = page.getOutlineInput();
//		if (!(rootObject instanceof IAdtOutlineTreeNode rootNode)) {
//			return null;
//		}

		ITreeContentProvider outlineContentProvider = new AdtOutlineTreeContentProvider(true, false);
		SourceRange currentSelectionRange = getRangeOfCurrentSelection(page);
		IDocument document = page.getDocument();
		URI sourceUri = getSourceUri(page.getFile());

		// Elements may be of type IAdtOutlineTreeNode, or IFile for the root, and
		// possibly anything inbetween.
		ArrayList<Object> nodes = new ArrayList<>();
		nodes.add(rootObject);
		return searchOutlineNodeForMethodImplementationRecursively(outlineContentProvider, nodes, currentSelectionRange,
				document, sourceUri);
	}

	public URI getSourceUri(IFile sourceFile) {
		IAdtUriMappingService service = AdtUriMappingServiceFactory.createUriMappingService();
		URI adtUri = service.getAdtUri(sourceFile);
		return adtUri;
	}

	private IObjectStructureElement getOutlineElement(Object node) {
		if (node instanceof IAdtOutlineTreeNode outlineNode
				&& outlineNode.getData() instanceof IObjectStructureElement outlineElement) {
			return outlineElement;
		}

		return null;
	}

	private SourceRange getRangeOfCurrentSelection(ITextEditor textEditor) {
		ITextSelection textSelection = getTextSelection(textEditor);
		if (textSelection == null) {
			return SourceRange.EMPTY;
		} else {
			return new SourceRange(textSelection.getOffset(), textSelection.getOffset() + textSelection.getLength());
		}
	}

	private ITextSelection getTextSelection(ITextEditor textEditor) {
		ISelection selection = textEditor.getSelectionProvider().getSelection();
		return selection != null && selection instanceof ITextSelection ? (ITextSelection) selection : null;
	}

	private ObjectSelection searchOutlineNodeForMethodImplementationRecursively(
			ITreeContentProvider outlineContentProvider, List<Object> nodes, SourceRange currentSelectionRange,
			IDocument document, URI sourceUri) {
		List<Object> childNodesForSameFile = new ArrayList<>();

		for (var node : nodes) {
			IObjectStructureElement outlineElement = getOutlineElement(node);
			if (outlineElement != null && isSameRelatedFile(outlineElement, sourceUri)) {

				if (sourceObjTypeUtil.isMethod(outlineElement)) {
					SourceRange implementationRange = getImplementationRange(outlineElement, document, sourceUri);
					if (implementationRange.contains(currentSelectionRange)) {
						return new ObjectSelection(outlineElement, implementationRange);
					}
				}

				if (isOutlineElementSurroundingEditorSelection(outlineElement, currentSelectionRange, document,
						sourceUri)) {
					Arrays.stream(outlineContentProvider.getChildren(node)).forEach(childNodesForSameFile::add);
				}
			} else {
				System.out.println(node.getClass());
			}
		}

		if (!childNodesForSameFile.isEmpty()) {
			return searchOutlineNodeForMethodImplementationRecursively(outlineContentProvider, childNodesForSameFile,
					currentSelectionRange, document, sourceUri);
		} else {
			ArrayList<Object> childNodesForOtherFiles = new ArrayList<>();

			for (var node : nodes) {
				Arrays.stream(outlineContentProvider.getChildren(node)).forEach(childNodesForOtherFiles::add);
			}

			return !childNodesForOtherFiles.isEmpty() ? searchOutlineNodeForMethodImplementationRecursively(
					outlineContentProvider, childNodesForOtherFiles, currentSelectionRange, document, sourceUri) : null;

		}
	}

	private boolean isOutlineElementSurroundingEditorSelection(IObjectStructureElement outlineElement,
			SourceRange currentSelectionRange, IDocument document, URI sourceUri) {
		SourceRange implementationRange = getImplementationRange(outlineElement, document, sourceUri);
		if (implementationRange.contains(currentSelectionRange)) {
			return true;
		}
		SourceRange definitionRange = getDefinitionRange(outlineElement, document, sourceUri);
		return definitionRange.contains(currentSelectionRange);
	}

	boolean isSameRelatedFile(IObjectStructureElement outlineElement, URI sourceUri) {
		URI definitionUri = outlineElement.getResolvedDefinitionUri();
		if (definitionUri != null && definitionUri.getPath().equals(sourceUri.getPath())) {
			return true;
		} else {
			URI implementationUri = outlineElement.getResolvedImplementationUri();
			return implementationUri != null && implementationUri.getPath().equals(sourceUri.getPath());
		}
	}

	SourceRange getImplementationRange(IObjectStructureElement element, IDocument document, URI sourceUri) {
		try {
			IAtomLink implBock = element.getLink("http://www.sap.com/adt/relations/source/implementationBlock");
			if (implBock != null) {
				IAtomLink implBaseLink = getBaseLink(element, "implementation");
				return createStartEndOffset(implBaseLink, implBock, document, sourceUri);
			}
			return SourceRange.EMPTY;
		} catch (BadLocationException | URISyntaxException var7) {
			return SourceRange.EMPTY;
		}
	}

	private SourceRange getDefinitionRange(IObjectStructureElement element, IDocument document, URI sourceUri) {
		try {
			IAtomLink defBlock = element.getLink("http://www.sap.com/adt/relations/source/definitionBlock");
			if (defBlock == null) {
				defBlock = element.getLink("definitionIdentifier");
				ISourceObjectTypeUtility utility = AbapSource.getInstance().createSourceObjectTypeUtility();
				if (utility.isInterfaceImplementation(element)) {
					defBlock = null;
				} else if (utility.isMethod(element)) {
					String name = element.getAttribute("name");
					if (name != null && name.contains("~")) {
						defBlock = null;
					}
				}
			}

			if (defBlock != null) {
				IAtomLink defBaseLink = getBaseLink(element, "definition");
				return createStartEndOffset(defBaseLink, defBlock, document, sourceUri);
			}
			return SourceRange.EMPTY;
		} catch (BadLocationException | URISyntaxException var8) {
			return SourceRange.EMPTY;
		}
	}

	private SourceRange createStartEndOffset(IAtomLink defBaseLink, IAtomLink defBlock, IDocument document,
			URI sourceUri) throws URISyntaxException, BadLocationException {
		URI resolved = defBaseLink.getHrefAsUri().resolve(defBlock.getHref());
		if (!resolved.toString().toLowerCase(Locale.ENGLISH)
				.startsWith(sourceUri.toString().toLowerCase(Locale.ENGLISH))) {
			return SourceRange.EMPTY;
		} else if (resolved.getFragment() == null) {
			return SourceRange.EMPTY;
		} else {
			IPlainTextFragmentHandler ptfh = ContentHandlingFactoryForTest.getPlainTextFragmentHandler();
			IPlainTextPosition textPosition = ptfh.parsePosition(resolved.getFragment());

			try {
				int start = document.getLineOffset(textPosition.getStartLine() - 1) + textPosition.getStartLineOffset();
				int end = document.getLineOffset(textPosition.getEndLine() - 1) + textPosition.getEndLineOffset();
				return new SourceRange(start, end);
			} catch (BadLocationException var10) {
				return SourceRange.EMPTY;
			}
		}
	}

	private IAtomLink getBaseLink(IObjectStructureElement el, String linkId) {
		if (el == null) {
			return null;
		} else {
			IAtomLink base = el.getBaseLink(linkId);
			return base != null ? base : getBaseLink(el.getParent(), linkId);
		}
	}
}
