package net.founderio.adtcodereview.model;

public class ReviewUserRequest {
	public String username;

	public ReviewUserRequest() {
		username = "";
	}

	public ReviewUserRequest(String username) {
		this.username = username;
	}
}
