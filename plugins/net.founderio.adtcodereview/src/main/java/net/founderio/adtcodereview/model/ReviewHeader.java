package net.founderio.adtcodereview.model;

import java.time.Instant;

public class ReviewHeader {
	/**
	 * Max length 3
	 */
	public String client;
	/**
	 * Max length 20, alphanumeric ID
	 */
	public String reviewId;
	/**
	 * Max length 12
	 */
	public String createdBy;
	/**
	 * Creation Timestamp
	 */
	public Instant createdOn;
	/**
	 * Max length 255
	 */
	public String title;
	public boolean lvorm;
}
