package net.founderio.adtcodereview.model;

import jakarta.annotation.Nullable;

public record SourceRange(int start, int end) {

	public static final SourceRange EMPTY = new SourceRange(-1, -1);

	public SourceRange(int start, int end) {
		if (start <= end) {
			this.start = start;
			this.end = end;
		} else {
			// Swap if they are ordered the wrong way.
			// Makes the implementation of contains() easier.
			this.end = start;
			this.start = end;
		}
	}

	public boolean contains(@Nullable SourceRange other) {
		if (other == null) {
			return false;
		}
		return start <= other.start && end >= other.end;
	}
}
