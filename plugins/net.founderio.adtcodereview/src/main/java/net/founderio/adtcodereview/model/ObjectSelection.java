package net.founderio.adtcodereview.model;

public record ObjectSelection(String destination, ReferencedObject object) {
}
