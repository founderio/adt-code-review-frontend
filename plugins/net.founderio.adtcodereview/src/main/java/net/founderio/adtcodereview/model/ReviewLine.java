package net.founderio.adtcodereview.model;

public class ReviewLine {
	/**
	 * Max length 3
	 */
	public String client = "";
	/**
	 * Max length 20, alphanumeric ID
	 */
	public String reviewId = "";
	/**
	 * Unsigned 32bit integer, INT4 in the backend
	 */
	public long lineId = -1;
	/**
	 * Max length 12, zacr_author
	 */
	public String author = "";
	/**
	 * Unbounded string on backend. Meant to hold only one line of text for overview
	 * purposes.
	 */
	public String title = "";
	/**
	 * Unbounded string on backend. Multi-line longtext description.
	 */
	public String longText = "";
	public ReferencedObject referencedObject = new ReferencedObject();
	/**
	 * Max length 12, zacr_assignee
	 */
	public String assignee = "";
	public boolean completed = false;

	public boolean hasAuthor() {
		return author != null && !author.isBlank();
	}
}
