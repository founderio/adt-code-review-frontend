package net.founderio.adtcodereview;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.swt.widgets.Shell;

import com.sap.adt.tools.core.system.IAbapSystemInfo;
import com.sap.adt.tools.core.system.IUser;
import com.sap.adt.tools.core.ui.userinfo.AdtUserServiceUIFactory;

import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import jakarta.inject.Inject;
import net.founderio.adtcodereview.model.User;

@SuppressWarnings("restriction")
@Creatable
public class AdtUserAccess {

	@Inject
	private AdtProjectAccess projectAccess;

	public User getUser(@Nullable IProgressMonitor monitor, @Nonnull String destination, @Nonnull String user) {
		IAbapSystemInfo systemInfo = projectAccess.getSystemInfo(destination);
		IUser adtUser = systemInfo.getUser(monitor, user);
		return new User(adtUser.getId(), adtUser.getText());
	}

	public String[] selectUserNamesFromDialog(@Nonnull Shell shell, boolean multipleSelection,
			@Nonnull IProject fixedProject, @Nullable String initialSearchString) {
		var userServiceUI = AdtUserServiceUIFactory.createAdtUserServiceUI();
		return userServiceUI.openUserNameSelectionDialog(shell, multipleSelection, fixedProject, initialSearchString);
	}
}
