package net.founderio.adtcodereview.parts;

import java.util.function.Consumer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.wb.swt.ResourceManager;

import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import net.founderio.adtcodereview.AdtObjectTypeAccess;
import net.founderio.adtcodereview.AdtProjectAccess;
import net.founderio.adtcodereview.model.ObjectSelection;
import net.founderio.adtcodereview.model.ReferencedObject;
import net.founderio.adtcodereview.model.ReviewLine;

public class ReviewLineEditWidget extends Composite {
	@Nullable
	private String destination;
	@Nullable
	private ReviewLine line;
	@Nullable
	private String referenceAuthor;

	AdtProjectAccess projectAccess;
	AdtObjectTypeAccess objectTypeAccess;

	private final Label lblID;
	private final Text txtReviewTitle;
	private final StyledText txtComment;
	private final ReferencedObjectWidget wgtReferencedObject;
	private final GridData gridDataReferencedObject;

	@Nullable
	private ObjectSelection selectedObjectInEditor;

	private Consumer<ReviewLine> cancelHandler;

	private Consumer<ReviewLine> saveHandler;

	private Consumer<ReviewLine> deleteHandler;
	private ToolBar toolBar;
	private ToolItem tltmCancel;
	private ToolItem tltmSave;
	private ToolItem tltmDelete;
	private ToolItem tltmLinkObject;
	private ToolItem tltmUnlinkObject;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public ReviewLineEditWidget(Composite parent, int style, AdtProjectAccess projectAccess,
			AdtObjectTypeAccess objectTypeAccess) {
		super(parent, style);
		this.projectAccess = projectAccess;
		this.objectTypeAccess = objectTypeAccess;
		setLayout(new GridLayout(3, false));

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 3, 1));

		tltmCancel = new ToolItem(toolBar, SWT.NONE);
		tltmCancel.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
			if (cancelHandler != null && line != null) {
				cancelHandler.accept(line);
			}
		}));
		tltmCancel.setToolTipText("Cancel Editing (reloads the original content from backend)");
		tltmCancel.setImage(ResourceManager.getPluginImage("net.founderio.adtcodereview", "icons/nav_backward.png"));

		tltmSave = new ToolItem(toolBar, SWT.NONE);
		tltmSave.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
			if (saveHandler != null && line != null) {
				saveHandler.accept(line);
			}
		}));
		tltmSave.setToolTipText("Save review line on backend");
		tltmSave.setImage(ResourceManager.getPluginImage("net.founderio.adtcodereview", "icons/save_edit.png"));

		tltmDelete = new ToolItem(toolBar, SWT.NONE);
		tltmDelete.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
			if (deleteHandler != null && line != null) {
				deleteHandler.accept(line);
			}
		}));
		tltmDelete.setToolTipText("Delete this review line");
		tltmDelete.setImage(ResourceManager.getPluginImage("net.founderio.adtcodereview", "icons/value_delete.png"));

		@SuppressWarnings("unused")
		ToolItem toolItem = new ToolItem(toolBar, SWT.SEPARATOR);

		tltmLinkObject = new ToolItem(toolBar, SWT.NONE);
		tltmLinkObject.addSelectionListener(SelectionListener.widgetSelectedAdapter(this::onLinkClicked));
		tltmLinkObject.setToolTipText("Link to currently selected development object or code line");
		tltmLinkObject.setImage(ResourceManager.getPluginImage("net.founderio.adtcodereview", "icons/link.png"));

		tltmUnlinkObject = new ToolItem(toolBar, SWT.NONE);
		tltmUnlinkObject.addSelectionListener(SelectionListener.widgetSelectedAdapter(this::onUnlinkClicked));
		tltmUnlinkObject.setToolTipText("Remove object link");
		tltmUnlinkObject
				.setImage(ResourceManager.getPluginImage("net.founderio.adtcodereview", "icons/link_delete.png"));

		Label lblNewLabel = new Label(this, SWT.WRAP);
		lblNewLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 3, 1));
		lblNewLabel.setText(
				"You are editing a single line in this review.\nCou can describe an issue, add a suggestion,\nask for feedback, etc.\n\nYou can also link your comment to\na whole development object,\na line of code or a range of lines.");

		lblID = new Label(this, SWT.NONE);
		lblID.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_DISABLED_FOREGROUND));
		lblID.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblID.setText("ID");

		txtReviewTitle = new Text(this, SWT.BORDER);
		txtReviewTitle.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		txtReviewTitle.addModifyListener(e -> {
			if (line == null) {
				txtReviewTitle.setText("");
				return;
			}
			line.title = txtReviewTitle.getText();
		});

		wgtReferencedObject = new ReferencedObjectWidget(this, SWT.NONE, projectAccess, objectTypeAccess);
		gridDataReferencedObject = new GridData(SWT.FILL, SWT.TOP, true, false, 3, 2);
		wgtReferencedObject.setLayoutData(gridDataReferencedObject);

		txtComment = new StyledText(this, SWT.BORDER | SWT.MULTI);
		txtComment.setAlwaysShowScrollBars(false);
		txtComment.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
		txtComment.addModifyListener(e -> {
			if (line == null) {
				txtComment.setText("");
				return;
			}
			line.longText = txtComment.getText();
		});
	}

	private void onLinkClicked(SelectionEvent e) {
		if (selectedObjectInEditor == null) {
			return;
		}
		if (destination.equals(selectedObjectInEditor.destination())) {
			line.referencedObject = selectedObjectInEditor.object();
			refresh();
		} else {
			// TODO: Show message about mismatch
		}
	}

	private void onUnlinkClicked(SelectionEvent e) {
		line.referencedObject = new ReferencedObject();
		refresh();
	}

	public ReviewLine getReviewLine() {
		return line;
	}

	public void setReviewLine(@Nonnull String destination, @Nonnull ReviewLine line) {
		this.destination = destination;
		this.line = line;
	}

	public void setCancelHandler(Consumer<ReviewLine> handler) {
		this.cancelHandler = handler;
	}

	public void setSaveHandler(Consumer<ReviewLine> handler) {
		this.saveHandler = handler;
	}

	public void setDeleteHandler(Consumer<ReviewLine> handler) {
		this.deleteHandler = handler;
	}

	public void clearReviewLine() {
		this.destination = null;
		this.line = null;
	}

	public void setSelectedObjectInEditor(@Nullable ObjectSelection selectedObjectInEditor) {
		this.selectedObjectInEditor = selectedObjectInEditor;
	}

	public void refresh() {
		if (line == null) {
			return;
		}

		if (line.lineId >= 0) {
			lblID.setText(Long.toString(line.lineId));
		} else {
			lblID.setText("<new line>");
		}

		txtReviewTitle.setText(line.title);

		txtComment.setText(line.longText);
		wgtReferencedObject.setObject(destination, line.referencedObject);
		gridDataReferencedObject.exclude = line.referencedObject == null || !line.referencedObject.isSet();

		tltmUnlinkObject.setEnabled(line.referencedObject != null && line.referencedObject.isSet());
		tltmDelete.setEnabled(line.lineId >= 0);

		layout(true, true);
	}

}
