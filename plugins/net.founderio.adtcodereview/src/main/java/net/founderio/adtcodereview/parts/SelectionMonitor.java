package net.founderio.adtcodereview.parts;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import net.founderio.adtcodereview.AdtObjectTypeAccess;
import net.founderio.adtcodereview.AdtProjectAccess;
import net.founderio.adtcodereview.AdtSelectionAccess;

public class SelectionMonitor {

	@Inject
	AdtSelectionAccess selectionAccess;
	@Inject
	AdtProjectAccess projectAccess;
	@Inject
	AdtObjectTypeAccess objectTypeAccess;

	private ReferencedObjectWidget wgtReferencedObject;
	private Label lblURL;

	private boolean ready;

	@PostConstruct
	public void createPartControl(Composite parent) {
		parent.setLayout(new GridLayout(1, false));

		lblURL = new Label(parent, SWT.NONE);
		lblURL.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		lblURL.setText("<Nothing Selected>");

		wgtReferencedObject = new ReferencedObjectWidget(parent, SWT.NONE, projectAccess, objectTypeAccess);
		wgtReferencedObject.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		ready = true;
	}

	@Inject
	void setSelection(@Optional @Named(IServiceConstants.ACTIVE_SELECTION) Object contact) {
		if (!ready) {
			return;
		}

		System.out.println("Selection: " + contact);
		if (contact != null) {
			System.out.println("Selection Class: " + contact.getClass());
		}

		var selectedObject = selectionAccess.getObjectFromSelection(contact);
		if (selectedObject == null || selectedObject.object() == null) {
			wgtReferencedObject.setObject("", null);
			lblURL.setText("<Nothing Selected>");
		} else {
			wgtReferencedObject.setObject(selectedObject.destination(), selectedObject.object());
			lblURL.setText(selectedObject.object().adtUri);
		}
	}
}
