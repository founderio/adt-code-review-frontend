package net.founderio.adtcodereview.extensions;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.EclipseContextFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.services.events.IEventBroker;

import com.sap.adt.destinations.logon.notification.ILoggedOnEvent;
import com.sap.adt.destinations.logon.notification.ILogonListener;

import jakarta.inject.Inject;
import net.founderio.adtcodereview.Activator;
import net.founderio.adtcodereview.EventTopics;

public class LogonListener implements ILogonListener {

	@Inject
	IEventBroker eventBroker;

	public LogonListener() {
		IEclipseContext context = EclipseContextFactory.getServiceContext(Activator.getContext());
//		IEclipseContext context = PlatformUI.getWorkbench().getService(IEclipseContext.class);
//		IEclipseContext context = E4Workbench.getServiceContext();
		ContextInjectionFactory.inject(this, context);
	}

	@Override
	public void loggedOn(ILoggedOnEvent event, IProgressMonitor progressMonitor) {
		eventBroker.post(EventTopics.TOPIC_ADT_LOGON, event.getDestinationData());
	}

}
