package net.founderio.adtcodereview;

import java.net.URI;

import org.eclipse.e4.core.di.annotations.Creatable;

import com.sap.adt.communication.resources.AdtRestResourceFactory;
import com.sap.adt.communication.resources.IRestResource;
import com.sap.adt.communication.resources.IRestResourceFactory;

import net.founderio.adtcodereview.model.PeopleResponse;
import net.founderio.adtcodereview.model.ReviewLineResponse;
import net.founderio.adtcodereview.model.ReviewLinesCreateRequest;
import net.founderio.adtcodereview.model.ReviewLinesSetCompletedRequest;
import net.founderio.adtcodereview.model.ReviewResponse;
import net.founderio.adtcodereview.model.ReviewUserRequest;
import net.founderio.adtcodereview.model.ReviewsCreateRequest;
import net.founderio.adtcodereview.model.ReviewsResponse;

@Creatable
public class ReviewsRepository {

	private IRestResourceFactory restResourceFactory;

	public ReviewsRepository() {
		restResourceFactory = AdtRestResourceFactory.createRestResourceFactory();
	}

	// TODO: Use com.sap.adt.communication.resources.UriBuilder to create the URIs
	// used below (after implementing unit tests)

	public ReviewsResponse getReviews(String destination) throws RuntimeException {
		URI reviewsUri = URI.create("/zacr/reviews");
		IRestResource reviewsResource = restResourceFactory.createResourceWithStatelessSession(reviewsUri, destination);
		try {
			reviewsResource.addContentHandler(new JacksonContentHandler<>(ReviewsResponse.class));
			// Trigger GET request on resource data
			return reviewsResource.get(null, ReviewsResponse.class);
		} catch (RuntimeException e) {
			throw new RuntimeException("Error retrieving review", e);
		}
	}

	public ReviewResponse getReview(String destination, String reviewId) throws RuntimeException {
		URI reviewsUri = URI.create("/zacr/reviews/" + reviewId);
		IRestResource reviewsResource = restResourceFactory.createResourceWithStatelessSession(reviewsUri, destination);
		try {
			reviewsResource.addContentHandler(new JacksonContentHandler<>(ReviewResponse.class));
			// Trigger GET request on resource data
			return reviewsResource.get(null, ReviewResponse.class);
		} catch (RuntimeException e) {
			throw new RuntimeException("Error retrieving reviews", e);
		}
	}

	public ReviewResponse createReview(String destination, ReviewsCreateRequest request) throws RuntimeException {
		URI reviewsUri = URI.create("/zacr/reviews");
		IRestResource reviewsResource = restResourceFactory.createResourceWithStatelessSession(reviewsUri, destination);
		try {
			reviewsResource.addContentHandler(new JacksonContentHandler<>(ReviewResponse.class));
			reviewsResource.addContentHandler(new JacksonContentHandler<>(ReviewsCreateRequest.class));
			// Trigger POST request on resource data
			return reviewsResource.post(null, ReviewResponse.class, request);
		} catch (RuntimeException e) {
			throw new RuntimeException("Error creating review", e);
		}
	}

	public ReviewLineResponse createReviewLine(String destination, String reviewId, ReviewLinesCreateRequest request)
			throws RuntimeException {
		URI reviewsUri = URI.create("/zacr/reviews/" + reviewId + "/lines");
		IRestResource reviewsResource = restResourceFactory.createResourceWithStatelessSession(reviewsUri, destination);
		try {
			reviewsResource.addContentHandler(new JacksonContentHandler<>(ReviewLineResponse.class));
			reviewsResource.addContentHandler(new JacksonContentHandler<>(ReviewLinesCreateRequest.class));
			// Trigger POST request on resource data
			return reviewsResource.post(null, ReviewLineResponse.class, request);
		} catch (RuntimeException e) {
			throw new RuntimeException("Error creating review line", e);
		}
	}

	public ReviewLineResponse updateReviewLine(String destination, String reviewId, long lineId,
			ReviewLinesCreateRequest request) throws RuntimeException {
		URI reviewsUri = URI.create("/zacr/reviews/" + reviewId + "/lines/" + lineId);
		IRestResource reviewsResource = restResourceFactory.createResourceWithStatelessSession(reviewsUri, destination);
		try {
			reviewsResource.addContentHandler(new JacksonContentHandler<>(ReviewLineResponse.class));
			reviewsResource.addContentHandler(new JacksonContentHandler<>(ReviewLinesCreateRequest.class));
			// Trigger POST request on resource data
			return reviewsResource.post(null, ReviewLineResponse.class, request);
		} catch (RuntimeException e) {
			throw new RuntimeException("Error updating review line", e);
		}
	}

	public void deleteReviewLine(String destination, String reviewId, long lineId) throws RuntimeException {
		URI reviewsUri = URI.create("/zacr/reviews/" + reviewId + "/lines/" + lineId);
		IRestResource reviewsResource = restResourceFactory.createResourceWithStatelessSession(reviewsUri, destination);
		try {
			// Trigger DELETE request on resource data
			reviewsResource.delete(null);
		} catch (RuntimeException e) {
			throw new RuntimeException("Error deleting review line", e);
		}
	}

	public ReviewLineResponse setReviewLineAssignee(String destination, String reviewId, long lineId,
			ReviewUserRequest request) throws RuntimeException {
		URI reviewsUri = URI.create("/zacr/reviews/" + reviewId + "/lines/" + lineId + "/assignee");
		IRestResource reviewsResource = restResourceFactory.createResourceWithStatelessSession(reviewsUri, destination);
		try {
			reviewsResource.addContentHandler(new JacksonContentHandler<>(ReviewLineResponse.class));
			reviewsResource.addContentHandler(new JacksonContentHandler<>(ReviewUserRequest.class));
			// Trigger POST request on resource data
			return reviewsResource.post(null, ReviewLineResponse.class, request);
		} catch (RuntimeException e) {
			throw new RuntimeException("Error setting review line assignee", e);
		}
	}

	public ReviewLineResponse setReviewLineCompleted(String destination, String reviewId, long lineId,
			ReviewLinesSetCompletedRequest request) throws RuntimeException {
		URI reviewsUri = URI.create("/zacr/reviews/" + reviewId + "/lines/" + lineId + "/completed");
		IRestResource reviewsResource = restResourceFactory.createResourceWithStatelessSession(reviewsUri, destination);
		try {
			reviewsResource.addContentHandler(new JacksonContentHandler<>(ReviewLineResponse.class));
			reviewsResource.addContentHandler(new JacksonContentHandler<>(ReviewLinesSetCompletedRequest.class));
			// Trigger POST request on resource data
			return reviewsResource.post(null, ReviewLineResponse.class, request);
		} catch (RuntimeException e) {
			throw new RuntimeException("Error setting review line completed", e);
		}
	}

	public PeopleResponse getReviewPeople(String destination, String reviewId) throws RuntimeException {
		URI reviewsUri = URI.create("/zacr/reviews/" + reviewId + "/people");
		IRestResource reviewsResource = restResourceFactory.createResourceWithStatelessSession(reviewsUri, destination);
		try {
			reviewsResource.addContentHandler(new JacksonContentHandler<>(PeopleResponse.class));
			// Trigger GET request on resource data
			return reviewsResource.get(null, PeopleResponse.class);
		} catch (RuntimeException e) {
			throw new RuntimeException("Error getting review people", e);
		}
	}

	public PeopleResponse addReviewReviewer(String destination, String reviewId, ReviewUserRequest request)
			throws RuntimeException {
		URI reviewsUri = URI.create("/zacr/reviews/" + reviewId + "/people/reviewer");
		IRestResource reviewsResource = restResourceFactory.createResourceWithStatelessSession(reviewsUri, destination);
		try {
			reviewsResource.addContentHandler(new JacksonContentHandler<>(PeopleResponse.class));
			reviewsResource.addContentHandler(new JacksonContentHandler<>(ReviewUserRequest.class));
			// Trigger POST request on resource data
			return reviewsResource.post(null, PeopleResponse.class, request);
		} catch (RuntimeException e) {
			throw new RuntimeException("Error adding review reviewer", e);
		}
	}

	public void deleteReviewReviewer(String destination, String reviewId, String username) throws RuntimeException {

		URI reviewsUri = URI.create("/zacr/reviews/" + reviewId + "/people/reviewer/" + username);
		IRestResource reviewsResource = restResourceFactory.createResourceWithStatelessSession(reviewsUri, destination);
		try {
			// Trigger POST request on resource data
			reviewsResource.delete(null);
		} catch (RuntimeException e) {
			throw new RuntimeException("Error deleting review reviewer", e);
		}
	}

	public PeopleResponse addReviewAudience(String destination, String reviewId, ReviewUserRequest request)
			throws RuntimeException {
		URI reviewsUri = URI.create("/zacr/reviews/" + reviewId + "/people/audience");
		IRestResource reviewsResource = restResourceFactory.createResourceWithStatelessSession(reviewsUri, destination);
		try {
			reviewsResource.addContentHandler(new JacksonContentHandler<>(PeopleResponse.class));
			reviewsResource.addContentHandler(new JacksonContentHandler<>(ReviewUserRequest.class));
			// Trigger POST request on resource data
			return reviewsResource.post(null, PeopleResponse.class, request);
		} catch (RuntimeException e) {
			throw new RuntimeException("Error adding review audience", e);
		}
	}

	public void deleteReviewAudience(String destination, String reviewId, String username) throws RuntimeException {

		URI reviewsUri = URI.create("/zacr/reviews/" + reviewId + "/people/audience/" + username);
		IRestResource reviewsResource = restResourceFactory.createResourceWithStatelessSession(reviewsUri, destination);
		try {
			// Trigger POST request on resource data
			reviewsResource.delete(null);
		} catch (RuntimeException e) {
			throw new RuntimeException("Error deleting review audience", e);
		}
	}
}
