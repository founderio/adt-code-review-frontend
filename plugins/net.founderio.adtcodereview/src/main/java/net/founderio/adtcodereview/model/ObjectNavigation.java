package net.founderio.adtcodereview.model;

import java.net.URI;

public record ObjectNavigation(String destination, URI adtURI) {
}
