package net.founderio.adtcodereview.model;

import java.util.List;

public class ReviewResponse {
	public ReviewHeader review;
	public List<ReviewLine> lines;
	public List<String> reviewers;
	public List<String> audience;
}
