package net.founderio.adtcodereview;

import org.eclipse.core.resources.IFile;
import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.part.MultiPageEditorPart;

import com.sap.adt.communication.internal.content.plaintext.SourceSemanticPosition;
import com.sap.adt.messageclass.model.messageclass.impl.MessageClassImpl;
import com.sap.adt.messageclass.model.messageclass.impl.MessageImpl;
import com.sap.adt.projectexplorer.ui.node.IAbapRepositoryObjectNode;
import com.sap.adt.tools.abapsource.sources.objectstructure.IObjectStructureElement;
import com.sap.adt.tools.abapsource.ui.internal.sources.editors.CompoundTextSelection;
import com.sap.adt.tools.abapsource.ui.sources.editors.IAbapSourcePage;
import com.sap.adt.tools.abapsource.ui.sources.outline.IAdtOutlineTreeNode;
import com.sap.adt.tools.core.IAdtObjectReference;
import com.sap.adt.tools.core.model.adtcore.IAdtObject;
import com.sap.adt.tools.core.ui.editors.IAdtFormEditor;
import com.sap.adt.tools.core.ui.internal.navigation.ITextNavigationSource;

import jakarta.inject.Inject;
import net.founderio.adtcodereview.model.ObjectNavigation;
import net.founderio.adtcodereview.model.ObjectSelection;
import net.founderio.adtcodereview.model.ReferencedObject;
import net.founderio.adtcodereview.model.SourceRange;

@SuppressWarnings("restriction")
@Creatable
public class AdtSelectionAccess {

	@Inject
	IWorkbench workbench;

	@Inject
	AdtProjectAccess projectAccess;

	@Inject
	AdtSourceOutlineAccess sourceOutlineAccess;

	public ObjectSelection getObjectFromSelection(Object contact) {
		if (contact == null) {
			return null;
		}

		if (contact instanceof IStructuredSelection structuredSelection) {
			return getObjectFromSelection(structuredSelection);
		} else if (contact instanceof CompoundTextSelection cts) {
			return getObjectFromSelection(cts);
		} else {
			return null;
		}
	}

	public ObjectSelection getObjectFromSelection(CompoundTextSelection cts) {
		System.out.println("ADT Object: " + cts.getRootModelEntity());

		// Get sub-object from source, if we can
		if (cts.getPart() instanceof MultiPageEditorPart mpep
				&& mpep.getSelectedPage() instanceof IAbapSourcePage sourcePage) {

			var selectedOutlineElement = sourceOutlineAccess
					.getOutlineNodeForMethodSurroundingEditorSelection(sourcePage);

			var objectNav = getObjectURIFromEditor(mpep);

			ReferencedObject selectedObject;

			if (selectedOutlineElement == null) {
				selectedObject = getObject(cts.getRootModelEntity());
			} else {
				selectedObject = getObject(cts.getRootModelEntity(), selectedOutlineElement.element());
			}

			if (cts.getStartLine() >= 0) {
				selectedObject.relativeLine = cts.getStartLine();
				var relevantLines = sourcePage.getSource().lines().skip(cts.getStartLine())
						.limit(cts.getEndLine() - cts.getStartLine() + 1);
				selectedObject.sourceCodeExtract = String.join("\n", relevantLines.toList());
			}

			// Adjust URI to navigate to enclosed object (e.g. Method) by name, not by line,
			// to make navigation more robust
			// /sap/bc/adt/oo/classes/cl_gui_test_operation_center/source/main#type=CLAS%2FOM;name=IF_WB_PROGRAM%7eGET_CURRENT_WB_OBJECT_STATE
			// /sap/bc/adt/oo/classes/cl_wb_request/source/main#start=444,13;end=445,15

			ITextNavigationSource navSource = mpep.getAdapter(ITextNavigationSource.class);
			if (navSource != null) {
				if (selectedObject.enclosedObjectType == "") {
					var uri = navSource.getResourceUri(/* include fragment */true, /* build context */true);
					selectedObject.adtUri = uri.toString();
				} else {
					var uri = navSource.getResourceUri(/* include fragment */false, /* build context */true);
					// The outline link service gives us the type method definition (CLAS/OO),
					// but we want to navigate into the method implementation (CLAS/OM),
					// because that is what the outline link service above actually uses to identify
					// enclosing scopes (definition never shows up).
					// TODO: Adjust our reimplementation of the outline link service
					// to handle both definition and implementation and return the appropriate
					// object
					if ("CLAS/OO".equals(selectedObject.enclosedObjectType)) {
						selectedObject.enclosedObjectType = "CLAS/OM";
					}

					SourceSemanticPosition ssp;
					if (selectedOutlineElement == null) {
						ssp = getRelativePosition(sourcePage.getDocument(), selectedObject);
					} else {
						ssp = getRelativePosition(cts, sourcePage.getDocument(), selectedOutlineElement.elementRange(),
								selectedObject);
					}

					selectedObject.adtUri = uri.toString() + "#" + ssp.toFragment();
				}
			}

			// Fall back to navigating to the editor
			if (selectedObject.adtUri == null) {
				selectedObject.adtUri = objectNav.adtURI().toString();
			}
			return new ObjectSelection(objectNav.destination(), selectedObject);
		} else {
			// Cannot get editor or active page
			return null;
		}
	}

	public SourceSemanticPosition getRelativePosition(CompoundTextSelection cts, IDocument document,
			SourceRange elementRange, ReferencedObject selectedObject) {
		if (cts.getStartLine() >= 0 && elementRange.start() >= 0) {
			try {
				int elementStartLine = document.getLineOfOffset(elementRange.start());

				int startLine = cts.getStartLine() - elementStartLine + 1;
				int startOffset = cts.getOffset() - document.getLineOffset(cts.getStartLine());
				int endLine = cts.getEndLine() - elementStartLine + 1;
				int endOffset = cts.getOffset() + cts.getLength() - document.getLineOffset(cts.getEndLine());
				return new SourceSemanticPosition(selectedObject.enclosedObjectType, selectedObject.enclosedObjectName,
						startLine, startOffset, endLine, endOffset);
			} catch (BadLocationException e) {
				return new SourceSemanticPosition(selectedObject.enclosedObjectType, selectedObject.enclosedObjectName);
			}
		} else {
			return new SourceSemanticPosition(selectedObject.enclosedObjectType, selectedObject.enclosedObjectName);
		}
	}

	public SourceSemanticPosition getRelativePosition(IDocument document, ReferencedObject selectedObject) {
		return new SourceSemanticPosition(selectedObject.enclosedObjectType, selectedObject.enclosedObjectName);
	}

	public ObjectSelection getObjectFromSelection(IStructuredSelection structuredSelection) {
		var firstElement = structuredSelection.getFirstElement();
		if (firstElement == null) {
			return null;
		}

		if (firstElement instanceof IAdtObject sub) {
			// Selection in Editor

			IEditorPart activeEditor = workbench.getActiveWorkbenchWindow().getActivePage().getActiveEditor();

			if (!(activeEditor instanceof MultiPageEditorPart adtEditor)) {
				// Can't navigate here
				return null;
			}

			var objectNav = getObjectURIFromEditor(adtEditor);

			ReferencedObject selectedObject;

			// Selection in MessageClass editor
			if (sub instanceof MessageImpl msg) {
				MessageClassImpl msgClass = (MessageClassImpl) msg.eContainer();
				selectedObject = getObject(msgClass, msg);
				// MessageImpl does not expose a type, but we know there is one
				// from cl_wb_registry=>get_objtype_provider( )->get_objtypes( ).
				selectedObject.enclosedObjectName = msg.getMsgno();
				selectedObject.enclosedObjectType = "MSAG/NN";
				// /sap/bc/adt/messageclass/zacr
				// /sap/bc/adt/messageclass/zacr/messages/901
				selectedObject.adtUri = objectNav.adtURI().toString() + String.format("/messages/%s", msg.getMsgno());
			} else {
				selectedObject = getObject(sub);
				selectedObject.adtUri = objectNav.adtURI().toString();
			}
			return new ObjectSelection(objectNav.destination(), selectedObject);

		} else if (firstElement instanceof IAdtOutlineTreeNode node
				&& node.getData() instanceof IObjectStructureElement outlineElement) {
			// Selection in project explorer (Outline nodes under classes)
			String objectDestination = projectAccess.getProjectDestination(node.getProject());
			var rootParent = outlineElement.getRootParent();

			ReferencedObject selectedObject = getObject(rootParent, outlineElement);
			return new ObjectSelection(objectDestination, selectedObject);
		} else if (firstElement instanceof IAbapRepositoryObjectNode node) {
			// Selection in project explorer (Packages, Data Dictionary, ...)
			String objectDestination = projectAccess.getProjectDestination(node.getProject());
			ReferencedObject selectedObject = getObject(node.getNavigationTarget());
			return new ObjectSelection(objectDestination, selectedObject);
		} else {
			System.out.println("First Element: " + firstElement + " class: " + firstElement.getClass());
			return null;
		}
	}

	public ObjectNavigation getObjectURIFromEditor(MultiPageEditorPart editor) {
		var navSource = editor.getAdapter(ITextNavigationSource.class);
		if (navSource != null) {
			var objectUri = navSource.getResourceUri(true, true);
			var objectDestination = navSource.getDestination();
			return new ObjectNavigation(objectDestination, objectUri);
		}
		/*
		 * else if (adtEditor.getSelectedPage() instanceof IAbapSourcePage sourcePage) {
		 * var oli = sourcePage.getOutlineInput(); }
		 */
		var file = findSelectedFileInEditor(editor);
		var objectUri = sourceOutlineAccess.getSourceUri(file);
		var objectDestination = projectAccess.getProjectDestination(file.getProject());
		return new ObjectNavigation(objectDestination, objectUri);
	}

	private static IFile findSelectedFileInEditor(final MultiPageEditorPart adtEditor) {
		if (adtEditor instanceof IAdtFormEditor fe) {
			IFile f = fe.getFile(adtEditor.getActivePage());
			return f;
		}
//		IEditorInput input = adtEditor.getEditorInput();
//	    if (input instanceof org.eclipse.ui.IFileEditorInput fei) {
//	      IFile file = fei.getFile();
//	      return file;
//	    }
		return null;
	}

	private ReferencedObject getObject(IAdtObjectReference mainObject) {
		var object = new ReferencedObject();

		object.objectName = mainObject.getName();
		object.objectType = mainObject.getType();
		object.adtUri = mainObject.getUri().toString();

		return object;
	}

	private ReferencedObject getObject(IAdtObject mainObject) {
		var object = new ReferencedObject();

		object.objectName = mainObject.getName();
		object.objectType = mainObject.getType();

		return object;
	}

	private ReferencedObject getObject(IAdtObject mainObject, IAdtObject subObject) {
		var object = new ReferencedObject();
		object.objectName = mainObject.getName();
		object.objectType = mainObject.getType();

		if (subObject != null) {
			object.enclosedObjectName = subObject.getName();
			object.enclosedObjectType = subObject.getType();
		}

		return object;
	}

	private ReferencedObject getObject(IObjectStructureElement mainObject, IObjectStructureElement subObject) {
		var object = new ReferencedObject();
		object.objectName = mainObject.getName();
		object.objectType = mainObject.getType();

		object.enclosedObjectName = subObject.getName();
		object.enclosedObjectType = subObject.getType();

		// TODO: This is using plain text position, we should adjust and add semantic
		// position, like we do in editor selections
		object.adtUri = subObject.getResolvedImplementationUri().toString();

		return object;
	}

	private ReferencedObject getObject(IAdtObject mainObject, IObjectStructureElement subObject) {
		var object = new ReferencedObject();
		object.objectName = mainObject.getName();
		object.objectType = mainObject.getType();

		object.enclosedObjectName = subObject.getName();
		object.enclosedObjectType = subObject.getType();
		// Note: This is adjusted outside of this method, replacing the whole URI from
		// navsource and then adding semantic position
		object.adtUri = subObject.getResolvedImplementationUri().toString();

		return object;
	}

}
