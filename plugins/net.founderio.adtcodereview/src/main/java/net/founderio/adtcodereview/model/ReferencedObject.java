package net.founderio.adtcodereview.model;

/**
 * Backend structure zacr_object_ref
 */
public class ReferencedObject {
	/**
	 * seu_obj, 15 characters
	 */
	public String objectType = "";
	/**
	 * seu_name, 30 characters
	 */
	public String objectName = "";

	/**
	 * seu_obj, 15 characters
	 */
	public String enclosedObjectType = "";
	/**
	 * seu_name, 30 characters
	 */
	public String enclosedObjectName = "";

	/**
	 * zacr_relative_line, 32bit unsigned integer, INT4 in backend
	 */
	public long relativeLine;

	/**
	 * sadt_wb_uri, 1000 characters
	 */
	public String adtUri = "";
	/**
	 * abap.string(0), unbounded string
	 */
	public String sourceCodeExtract = "";

	public boolean isSet() {
		return !objectType.isEmpty();
	}
}
