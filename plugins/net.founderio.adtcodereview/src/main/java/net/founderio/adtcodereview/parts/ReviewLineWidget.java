package net.founderio.adtcodereview.parts;

import java.util.function.Consumer;

import org.eclipse.jface.resource.FontDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import net.founderio.adtcodereview.AdtObjectTypeAccess;
import net.founderio.adtcodereview.AdtProjectAccess;
import net.founderio.adtcodereview.AdtUserAccess;
import net.founderio.adtcodereview.model.ReviewLine;
import net.founderio.adtcodereview.model.User;

public class ReviewLineWidget extends Composite {
	private ResourceManager localResourceManager;

	AdtUserAccess userAccess;
	AdtProjectAccess projectAccess;
	AdtObjectTypeAccess objectTypeAccess;

	@Nullable
	private String destination;
	@Nullable
	private ReviewLine line;
	@Nullable
	private String referenceAuthor;
	private boolean editable;

	private Consumer<ReviewLine> editHandler;
	private Consumer<ReviewLine> deleteHandler;
	private Consumer<ReviewLine> assignHandler;
	private Consumer<ReviewLine> unassignHandler;
	private Consumer<ReviewLine> completeHandler;
	private Consumer<ReviewLine> uncompleteHandler;

	private final Label lblID;
	private final Label lblCompleted;
	private final GridData gridDataLblCompleted;
	private final Label lblReviewTitle;
	private final Label lblBy;
	private final Label lblAuthor;
	private final StyledText txtComment;
	private final ReferencedObjectWidget wgtReferencedObject;
	private final GridData gridDataReferencedObject;
	private final ToolBar toolBar;
	private final ToolItem tltmConfirmAsDone;
	private final ToolItem tltmEdit;
	private final ToolItem tltmDelete;
	private final ToolItem tltmAssign;
	private final Label lblAssignedTo;
	private final Label lblAssignee;

	private User assigneeCache;
	private User authorCache;

	/**
	 * Create the composite.
	 *
	 * @param parent
	 * @param style
	 */
	public ReviewLineWidget(Composite parent, int style, AdtUserAccess userAccess, AdtProjectAccess projectAccess,
			AdtObjectTypeAccess objectTypeAccess) {
		super(parent, style);
		this.userAccess = userAccess;
		this.projectAccess = projectAccess;
		this.objectTypeAccess = objectTypeAccess;
		createResourceManager();
		setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
		setLayout(new GridLayout(5, false));

		lblID = new Label(this, SWT.NONE);
		lblID.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_DISABLED_FOREGROUND));
		lblID.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblID.setText("ID");

		lblCompleted = new Label(this, SWT.NONE);
		gridDataLblCompleted = new GridData();
		lblCompleted.setLayoutData(gridDataLblCompleted);
		lblCompleted.setImage(org.eclipse.wb.swt.ResourceManager.getPluginImage("net.founderio.adtcodereview",
				"icons/complete_status.png"));

		lblReviewTitle = new Label(this, SWT.WRAP);
		lblReviewTitle.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
		lblReviewTitle.setFont(localResourceManager.create(FontDescriptor.createFrom("Noto Sans", 10, SWT.BOLD)));
		lblReviewTitle.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1));
		lblReviewTitle.setText("Review Title");

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));

		tltmConfirmAsDone = new ToolItem(toolBar, SWT.NONE);
		tltmConfirmAsDone.addSelectionListener(SelectionListener.widgetSelectedAdapter(this::onCompletedPressed));
		tltmConfirmAsDone.setToolTipText("Confirm as Done");
		tltmConfirmAsDone.setImage(org.eclipse.wb.swt.ResourceManager.getPluginImage("net.founderio.adtcodereview",
				"icons/tasks_tsk.png"));

		tltmAssign = new ToolItem(toolBar, SWT.NONE);
		tltmAssign.addSelectionListener(SelectionListener.widgetSelectedAdapter(this::onAssignPressed));
		tltmAssign.setImage(org.eclipse.wb.swt.ResourceManager.getPluginImage("net.founderio.adtcodereview",
				"icons/facet_user.png"));
		tltmAssign.setToolTipText("Assign to me");

		tltmEdit = new ToolItem(toolBar, SWT.NONE);
		tltmEdit.addSelectionListener(SelectionListener.widgetSelectedAdapter(this::onEditPressed));
		tltmEdit.setToolTipText("Edit");
		tltmEdit.setImage(org.eclipse.wb.swt.ResourceManager.getPluginImage("net.founderio.adtcodereview",
				"icons/edit_template.png"));

		tltmDelete = new ToolItem(toolBar, SWT.NONE);
		tltmDelete.addSelectionListener(SelectionListener.widgetSelectedAdapter(this::onDeletePressed));
		tltmDelete.setToolTipText("Delete this review line");
		tltmDelete.setImage(org.eclipse.wb.swt.ResourceManager.getPluginImage("net.founderio.adtcodereview",
				"icons/value_delete.png"));
		new Label(this, SWT.NONE);

		lblAssignedTo = new Label(this, SWT.NONE);
		lblAssignedTo.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 2, 1));
		lblAssignedTo.setText("Assigned to");

		lblAssignee = new Label(this, SWT.NONE);
		lblAssignee.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1));
		lblAssignee.setText("Assignee");

		wgtReferencedObject = new ReferencedObjectWidget(this, SWT.NONE, projectAccess, objectTypeAccess);
		gridDataReferencedObject = new GridData(SWT.FILL, SWT.CENTER, true, false, 5, 1);
		wgtReferencedObject.setLayoutData(gridDataReferencedObject);

		txtComment = new StyledText(this, SWT.NONE);
		txtComment.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
		txtComment.setEditable(false);
		txtComment.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 5, 1));
		new Label(this, SWT.NONE);
		new Label(this, SWT.NONE);

		lblBy = new Label(this, SWT.NONE);
		lblBy.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 2, 1));
		lblBy.setText("by");

		lblAuthor = new Label(this, SWT.NONE);
		lblAuthor.setText("Author");

	}

	private void createResourceManager() {
		localResourceManager = new LocalResourceManager(JFaceResources.getResources(), this);
	}

	private void onEditPressed(SelectionEvent e) {
		if (editable && line != null && editHandler != null) {
			editHandler.accept(line);
		}
	}

	private void onDeletePressed(SelectionEvent e) {
		if (editable && line != null && deleteHandler != null) {
			deleteHandler.accept(line);
		}
	}

	private void onAssignPressed(SelectionEvent e) {
		if (line != null && assignHandler != null) {
			if (hasAssignee()) {
				unassignHandler.accept(line);
			} else {
				assignHandler.accept(line);
			}
		}
	}

	private void onCompletedPressed(SelectionEvent e) {
		if (line != null && completeHandler != null) {
			if (line.completed) {
				uncompleteHandler.accept(line);
			} else {
				completeHandler.accept(line);
			}
		}
	}

	public void setReviewLine(@Nonnull String destination, @Nonnull ReviewLine line) {
		this.destination = destination;
		this.line = line;
	}

	public void clearReviewLine() {
		this.destination = null;
		this.line = null;
	}

	public void setReferenceAuthor(@Nullable String author) {
		this.referenceAuthor = author;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public void setEditHandler(Consumer<ReviewLine> handler) {
		this.editHandler = handler;
	}

	public void setDeleteHandler(Consumer<ReviewLine> handler) {
		this.deleteHandler = handler;
	}

	public void setAssignHandler(Consumer<ReviewLine> handler) {
		this.assignHandler = handler;
	}

	public void setUnassignHandler(Consumer<ReviewLine> handler) {
		this.unassignHandler = handler;
	}

	public void setCompleteHandler(Consumer<ReviewLine> handler) {
		this.completeHandler = handler;
	}

	public void setUncompleteHandler(Consumer<ReviewLine> handler) {
		this.uncompleteHandler = handler;
	}

	public boolean hasReferenceAuthor() {
		return referenceAuthor != null && !referenceAuthor.isBlank();
	}

	public boolean hasDifferentAuthor() {
		return line != null && line.hasAuthor() && (hasReferenceAuthor() || !referenceAuthor.equals(line.author));
	}

	private boolean hasAssignee() {
		return line != null && line.assignee != null && !line.assignee.isEmpty();
	}

	public void refresh() {
		if (line == null) {
			return;
		}

		lblID.setText(String.valueOf(line.lineId));
		lblReviewTitle.setText(line.title);
		var hasDifferentAuthor = hasDifferentAuthor();
		lblBy.setVisible(hasDifferentAuthor);
		lblAuthor.setVisible(hasDifferentAuthor);

		if (hasDifferentAuthor) {
			if ((authorCache == null || !line.author.equalsIgnoreCase(authorCache.id()))) {
				authorCache = userAccess.getUser(null, destination, line.author);
			}
			lblAuthor.setText(authorCache == null ? line.author : authorCache.text());
		} else {
			lblAuthor.setText("");
		}

		var hasAssignee = hasAssignee();
		if (hasAssignee) {
			if ((assigneeCache == null || !line.assignee.equalsIgnoreCase(assigneeCache.id()))) {
				assigneeCache = userAccess.getUser(null, destination, line.assignee);
			}
			lblAssignee.setText(assigneeCache == null ? line.assignee : assigneeCache.text());
		} else {
			lblAssignee.setText("");
		}
		lblAssignee.setVisible(hasAssignee);
		lblAssignedTo.setVisible(hasAssignee);

		txtComment.setText(line.longText);
		wgtReferencedObject.setObject(destination, line.referencedObject);
		gridDataReferencedObject.exclude = line.referencedObject == null || !line.referencedObject.isSet();

		tltmEdit.setEnabled(editable);
		tltmDelete.setEnabled(editable);

		if (hasAssignee) {
			tltmAssign.setImage(org.eclipse.wb.swt.ResourceManager.getPluginImage("net.founderio.adtcodereview",
					"icons/remove_user.png"));
			tltmAssign.setToolTipText("Unassign");
		} else {
			tltmAssign.setImage(org.eclipse.wb.swt.ResourceManager.getPluginImage("net.founderio.adtcodereview",
					"icons/facet_user.png"));
			tltmAssign.setToolTipText("Assign to me");
		}

		if (line.completed) {
			// Theme the whole widget to look disabled
			setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_DISABLED_FOREGROUND));

			tltmConfirmAsDone.setToolTipText("Mark as not done");
			tltmConfirmAsDone.setImage(org.eclipse.wb.swt.ResourceManager.getPluginImage("net.founderio.adtcodereview",
					"icons/tasks_not_done.png"));
		} else {
			// Restore normal foreground colours
			setForeground(null);

			tltmConfirmAsDone.setToolTipText("Confirm as done");
			tltmConfirmAsDone.setImage(org.eclipse.wb.swt.ResourceManager.getPluginImage("net.founderio.adtcodereview",
					"icons/tasks_tsk.png"));
		}
		// Show a checkmark, if completed
		lblCompleted.setVisible(line.completed);
		gridDataLblCompleted.exclude = !line.completed;

		layout(true, true);
	}

}
