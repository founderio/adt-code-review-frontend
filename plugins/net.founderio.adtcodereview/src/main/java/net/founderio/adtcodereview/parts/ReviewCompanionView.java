package net.founderio.adtcodereview.parts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.ICoreRunnable;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.ui.IWorkbench;
import org.eclipse.wb.swt.ResourceManager;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import net.founderio.adtcodereview.AdtObjectTypeAccess;
import net.founderio.adtcodereview.AdtProjectAccess;
import net.founderio.adtcodereview.AdtSelectionAccess;
import net.founderio.adtcodereview.AdtUserAccess;
import net.founderio.adtcodereview.ReviewsRepository;
import net.founderio.adtcodereview.model.PeopleResponse;
import net.founderio.adtcodereview.model.ReviewLine;
import net.founderio.adtcodereview.model.ReviewLineResponse;
import net.founderio.adtcodereview.model.ReviewLinesCreateRequest;
import net.founderio.adtcodereview.model.ReviewLinesSetCompletedRequest;
import net.founderio.adtcodereview.model.ReviewResponse;
import net.founderio.adtcodereview.model.ReviewTableEntry;
import net.founderio.adtcodereview.model.ReviewUserRequest;

public class ReviewCompanionView {
	public static final String PART_ID = "net.founderio.adtcodereview.ReviewCompanionView";

	private static final String noReviewSelected = "<no review selected>";

	private boolean ready = false;
	private boolean editMode = false;

	private ReviewTableEntry review;
	private final List<String> reviewers = new ArrayList<>();
	private final List<String> audience = new ArrayList<>();
	private final List<ReviewLine> comments = new ArrayList<>();

	private final List<ReviewLineWidget> commentsWidgets = new ArrayList<>();

	private ToolBar toolBar;
	private ToolItem tltmCloseReview;
	private ToolItem tltmCreateLine;
	private ToolItem tltmCreateReview;

	private Label lblAuthor;
	private Text txtID;
	private Text txtTitle;
	private Label lblDestination;

	private Composite composite;
	private ReviewLineEditWidget editingLineWidget;
	private Composite cmpBody;
	private ScrolledComposite cmpLinesScroll;
	private StackLayout cmpBodyLayout;
	private Composite cmpLines;

	@Inject
	UISynchronize sync;

	@Inject
	ReviewsRepository repository;

	@Inject
	AdtProjectAccess projectAccess;

	@Inject
	AdtUserAccess userAccess;

	@Inject
	AdtSelectionAccess selectionAccess;

	@Inject
	AdtObjectTypeAccess objectTypeAccess;

	@Inject
	IWorkbench workbench;
	private Label lblReviewers;
	private PeopleWidget wgtReviewers;
	private PeopleWidget wgtAudience;
	private Label lblAudience;

	public void setReview(ReviewTableEntry review) {
		this.review = review;

		refresh();
		refreshFromBackend();
	}

	private void refreshFromBackend() {
		clearEditingLine();

		if (review == null) {
			return;
		}

		Job.create("Load review content from Backend", (ICoreRunnable) monitor -> {
			ReviewResponse response = repository.getReview(review.destination, review.review.reviewId);

			sync.asyncExec(() -> {
				review.review = response.review;
				comments.clear();
				comments.addAll(response.lines);
				reviewers.clear();
				reviewers.addAll(response.reviewers);
				audience.clear();
				audience.addAll(response.audience);
				refresh();
			});
		}).schedule();
	}

	private void createComment(SelectionEvent e) {
		editingLineWidget.setReviewLine(review.destination, new ReviewLine());
		editingLineWidget.refresh();
		editMode = true;
		refresh();
	}

	private void closeReview(SelectionEvent e) {
		setReview(null);
	}

	private void createReview(SelectionEvent e) {

		Job.create("Create Review", (ICoreRunnable) monitor -> {
			var project = projectAccess.chooseAbapProject();
			if (project == null) {
				return;
			}

			String destinationId = projectAccess.getProjectDestination(project);
			var response = repository.createReview(destinationId, null);

			var newTableEntry = new ReviewTableEntry(destinationId, response.review);
			sync.asyncExec(() -> {
				setReview(newTableEntry);
			});
		}).schedule();
	}

	private void saveEditingLine(ReviewLine editingLine) {
		// Failsave, this should never happen
		if (!editMode || editingLine == null) {
			return;
		}

		Job.create("Create review line on backend", (ICoreRunnable) monitor -> {
			var request = new ReviewLinesCreateRequest();
			request.longText = editingLine.longText;
			request.title = editingLine.title;
			request.referencedObject = editingLine.referencedObject;

			if (editingLine.lineId > 0) {
				ReviewLineResponse response = repository.updateReviewLine(review.destination, review.review.reviewId,
						editingLine.lineId, request);
				int idx = comments.indexOf(editingLine);
				if (idx < 0) {
					// Failsafe
					comments.add(response.line);
				} else {
					comments.set(idx, response.line);
				}
			} else {
				ReviewLineResponse response = repository.createReviewLine(review.destination, review.review.reviewId,
						request);
				comments.add(response.line);
			}

			sync.asyncExec(() -> {
				clearEditingLine();
				refresh();
			});
		}).schedule();
	}

	private void clearEditingLine() {
		if (!editMode) {
			return;
		}
		editingLineWidget.clearReviewLine();
		editingLineWidget.refresh();
		editMode = false;
		refresh();
	}

	private void cancelEditingLine(ReviewLine editingLine) {
		clearEditingLine();
		refreshFromBackend();
	}

	private void deleteEditingLine(ReviewLine editingLine) {
		// Failsave, this should never happen
		if (!editMode || editingLine == null) {
			return;
		}

		if (!MessageDialog.openConfirm(new Shell(), "Delete this review line?",
				"This will delete the review line permanently. Continue?")) {
			return;
		}

		Job.create("Delete review line on backend", (ICoreRunnable) monitor -> {
			repository.deleteReviewLine(review.destination, review.review.reviewId, editingLine.lineId);

			comments.remove(editingLine);

			sync.asyncExec(() -> {
				clearEditingLine();
				refresh();
			});
		}).schedule();
	}

	private void assignEditingLine(ReviewLine editingLine) {
		// Failsave, this should never happen
		if (editingLine == null) {
			return;
		}

		Job.create("Assign review line", (ICoreRunnable) monitor -> {
			String user = projectAccess.getLoggedInUser(review.destination);

			var response = repository.setReviewLineAssignee(review.destination, review.review.reviewId,
					editingLine.lineId, new ReviewUserRequest(user));
			int idx = comments.indexOf(editingLine);
			if (idx < 0) {
				// Failsafe
				comments.add(response.line);
			} else {
				comments.set(idx, response.line);
			}

			sync.asyncExec(() -> {
				refresh();
			});
		}).schedule();
	}

	private void unassignEditingLine(ReviewLine editingLine) {
		// Failsave, this should never happen
		if (editingLine == null) {
			return;
		}

		Job.create("Unassign review line", (ICoreRunnable) monitor -> {
			var response = repository.setReviewLineAssignee(review.destination, review.review.reviewId,
					editingLine.lineId, new ReviewUserRequest(""));
			int idx = comments.indexOf(editingLine);
			if (idx < 0) {
				// Failsafe
				comments.add(response.line);
			} else {
				comments.set(idx, response.line);
			}

			sync.asyncExec(() -> {
				refresh();
			});
		}).schedule();
	}

	private void completeEditingLine(ReviewLine editingLine) {
		// Failsave, this should never happen
		if (editingLine == null) {
			return;
		}

		Job.create("Mark review line as completed", (ICoreRunnable) monitor -> {
			var response = repository.setReviewLineCompleted(review.destination, review.review.reviewId,
					editingLine.lineId, new ReviewLinesSetCompletedRequest(true));
			int idx = comments.indexOf(editingLine);
			if (idx < 0) {
				// Failsafe
				comments.add(response.line);
			} else {
				comments.set(idx, response.line);
			}

			sync.asyncExec(() -> {
				refresh();
			});
		}).schedule();
	}

	private void uncompleteEditingLine(ReviewLine editingLine) {
		// Failsave, this should never happen
		if (editingLine == null) {
			return;
		}

		Job.create("Mark review line as not completed", (ICoreRunnable) monitor -> {
			var response = repository.setReviewLineCompleted(review.destination, review.review.reviewId,
					editingLine.lineId, new ReviewLinesSetCompletedRequest(false));
			int idx = comments.indexOf(editingLine);
			if (idx < 0) {
				// Failsafe
				comments.add(response.line);
			} else {
				comments.set(idx, response.line);
			}

			sync.asyncExec(() -> {
				refresh();
			});
		}).schedule();
	}

	@PostConstruct
	public void createPartControl(Composite parent) {
		parent.setLayout(new FillLayout(SWT.VERTICAL));

		composite = new Composite(parent, SWT.NONE);
		composite.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_LIST_BACKGROUND));
		composite.setLayout(new GridLayout(1, false));

		Composite cmpReviewHeader = new Composite(composite, SWT.WRAP);
		cmpReviewHeader.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_LIST_BACKGROUND));
		cmpReviewHeader.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		GridLayout gl_cmpReviewHeader = new GridLayout();
		gl_cmpReviewHeader.numColumns = 4;
		cmpReviewHeader.setLayout(gl_cmpReviewHeader);

		toolBar = new ToolBar(cmpReviewHeader, SWT.FLAT | SWT.RIGHT);
		toolBar.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_LIST_BACKGROUND));
		toolBar.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 2, 2));

		tltmCloseReview = new ToolItem(toolBar, SWT.NONE);
		tltmCloseReview.addSelectionListener(SelectionListener.widgetSelectedAdapter(this::closeReview));
		tltmCloseReview.setToolTipText("Close this review");
		tltmCloseReview.setImage(ResourceManager.getPluginImage("net.founderio.adtcodereview", "icons/close_view.png"));

		tltmCreateReview = new ToolItem(toolBar, SWT.NONE);
		tltmCreateReview.addSelectionListener(SelectionListener.widgetSelectedAdapter(this::createReview));
		tltmCreateReview.setToolTipText("Create new review");
		tltmCreateReview
				.setImage(ResourceManager.getPluginImage("net.founderio.adtcodereview", "icons/newfile_wiz.png"));

		@SuppressWarnings("unused")
		ToolItem toolItem = new ToolItem(toolBar, SWT.SEPARATOR);

		tltmCreateLine = new ToolItem(toolBar, SWT.NONE);
		tltmCreateLine.addSelectionListener(SelectionListener.widgetSelectedAdapter(this::createComment));
		tltmCreateLine.setImage(ResourceManager.getPluginImage("net.founderio.adtcodereview", "icons/value_add.png"));
		tltmCreateLine.setToolTipText("Add new review line");

		lblDestination = new Label(cmpReviewHeader, SWT.NONE);
		lblDestination.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 2, 1));
		lblDestination.setText("Project");

		Label lblReviewBy = new Label(cmpReviewHeader, SWT.NONE);
		lblReviewBy.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
		lblReviewBy.setText("Review by");

		lblAuthor = new Label(cmpReviewHeader, SWT.NONE);
		lblAuthor.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblAuthor.setText("Author");

		txtID = new Text(cmpReviewHeader, SWT.READ_ONLY);
		txtID.setText("ID");
		txtID.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_DISABLED_FOREGROUND));
		txtID.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_LIST_BACKGROUND));
		txtID.setEditable(false);
		GridData gd_txtID = new GridData(GridData.FILL_HORIZONTAL);
		gd_txtID.horizontalAlignment = SWT.LEFT;
		gd_txtID.grabExcessHorizontalSpace = false;
		txtID.setLayoutData(gd_txtID);

		txtTitle = new Text(cmpReviewHeader, SWT.BORDER);
		txtTitle.setText("Review Title");
		txtTitle.setEditable(false);
		GridData gd_txtTitle = new GridData(GridData.FILL_HORIZONTAL);
		gd_txtTitle.horizontalSpan = 3;
		txtTitle.setLayoutData(gd_txtTitle);
		new Label(cmpReviewHeader, SWT.NONE);

		lblReviewers = new Label(cmpReviewHeader, SWT.NONE);
		lblReviewers.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		lblReviewers.setText("Reviewers");

		wgtReviewers = new PeopleWidget(cmpReviewHeader, SWT.NONE, userAccess, projectAccess);
		wgtReviewers.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, true, false, 2, 1));
		wgtReviewers.setAddHandler(this::onAddReviewers);
		wgtReviewers.setDeleteHandler(this::onDeleteReviewer);
		new Label(cmpReviewHeader, SWT.NONE);

		lblAudience = new Label(cmpReviewHeader, SWT.NONE);
		lblAudience.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		lblAudience.setText("Audience");

		wgtAudience = new PeopleWidget(cmpReviewHeader, SWT.NONE, userAccess, projectAccess);
		wgtAudience.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, true, false, 2, 1));
		wgtAudience.setAddHandler(this::onAddAudience);
		wgtAudience.setDeleteHandler(this::onDeleteAudience);

		cmpBody = new Composite(composite, SWT.NONE);
		cmpBody.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		cmpBody.setSize(448, 271);
		cmpBodyLayout = new StackLayout();
		cmpBody.setLayout(cmpBodyLayout);

		cmpLinesScroll = new ScrolledComposite(cmpBody, SWT.V_SCROLL);
		cmpLinesScroll.setShowFocusedControl(true);
		cmpLinesScroll.setExpandHorizontal(true);
		cmpLinesScroll.setExpandVertical(true);

		cmpLines = new Composite(cmpLinesScroll, SWT.NONE);
		cmpLines.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_LIST_BACKGROUND));
		GridLayout gl_cmpLines = new GridLayout(1, false);
		gl_cmpLines.marginHeight = 0;
		gl_cmpLines.marginBottom = 10;
		gl_cmpLines.marginRight = 5;
		cmpLines.setLayout(gl_cmpLines);
		cmpLinesScroll.setContent(cmpLines);

		cmpLinesScroll.addListener(SWT.Resize, event -> updateScrollContentSize());

		editingLineWidget = new ReviewLineEditWidget(cmpBody, SWT.NONE, projectAccess, objectTypeAccess);
		editingLineWidget.setCancelHandler(this::cancelEditingLine);
		editingLineWidget.setSaveHandler(this::saveEditingLine);
		editingLineWidget.setDeleteHandler(this::deleteEditingLine);

		ready = true;

		refresh();
	}

	private void onAddReviewers(String[] users) {
		Job.create("Add users to reviewers", (ICoreRunnable) monitor -> {

			// TODO: Provide alternative with multiple users, to only process response once
			PeopleResponse lastResponse = null;
			for (String username : users) {
				lastResponse = repository.addReviewReviewer(review.destination, review.review.reviewId,
						new ReviewUserRequest(username));
			}
			reviewers.clear();
			reviewers.addAll(lastResponse.reviewers);
			audience.clear();
			audience.addAll(lastResponse.audience);

			sync.asyncExec(() -> {
				refresh();
			});
		}).schedule();
	}

	private void onDeleteReviewer(String username) {
		Job.create("Remove user from reviewers", (ICoreRunnable) monitor -> {
			repository.deleteReviewReviewer(review.destination, review.review.reviewId, username);
			PeopleResponse lastResponse = repository.getReviewPeople(review.destination, review.review.reviewId);
			reviewers.clear();
			reviewers.addAll(lastResponse.reviewers);
			audience.clear();
			audience.addAll(lastResponse.audience);

			sync.asyncExec(() -> {
				refresh();
			});
		}).schedule();
	}

	private void onAddAudience(String[] users) {
		Job.create("Add users to review audience", (ICoreRunnable) monitor -> {

			// TODO: Provide alternative with multiple users, to only process response once
			PeopleResponse lastResponse = null;
			for (String username : users) {
				lastResponse = repository.addReviewAudience(review.destination, review.review.reviewId,
						new ReviewUserRequest(username));
			}
			reviewers.clear();
			reviewers.addAll(lastResponse.reviewers);
			audience.clear();
			audience.addAll(lastResponse.audience);

			sync.asyncExec(() -> {
				refresh();
			});
		}).schedule();
	}

	private void onDeleteAudience(String username) {
		Job.create("Remove user from review audience", (ICoreRunnable) monitor -> {
			repository.deleteReviewAudience(review.destination, review.review.reviewId, username);
			PeopleResponse lastResponse = repository.getReviewPeople(review.destination, review.review.reviewId);
			reviewers.clear();
			reviewers.addAll(lastResponse.reviewers);
			audience.clear();
			audience.addAll(lastResponse.audience);

			sync.asyncExec(() -> {
				refresh();
			});
		}).schedule();
	}

	private void updateScrollContentSize() {
		int width = cmpLinesScroll.getClientArea().width;
		cmpLinesScroll.setMinSize(cmpLines.computeSize(width, SWT.DEFAULT));
	}

	public void refresh() {
		if (review == null) {
			txtID.setText(noReviewSelected);
			lblAuthor.setText(noReviewSelected);
			txtTitle.setText(noReviewSelected);
			lblDestination.setText(noReviewSelected);

			tltmCreateLine.setEnabled(false);

			clearEditingLine();
			editingLineWidget.setSelectedObjectInEditor(null);

			for (ReviewLineWidget widget : commentsWidgets) {
				widget.dispose();
			}
			commentsWidgets.clear();

			reviewers.clear();
			wgtReviewers.setUsers("", reviewers);
			audience.clear();
			wgtAudience.setUsers("", audience);
		} else {
			txtID.setText(review.review.reviewId);
			lblAuthor.setText(review.review.createdBy);
			txtTitle.setText(review.review.title);
			lblDestination.setText(review.destination);

			tltmCreateLine.setEnabled(!editMode);

			if (editMode) {
				cmpBodyLayout.topControl = editingLineWidget;
			} else {
				cmpBodyLayout.topControl = cmpLinesScroll;
			}

			while (commentsWidgets.size() > comments.size()) {
				int lastIndex = commentsWidgets.size() - 1;
				commentsWidgets.get(lastIndex).dispose();
				commentsWidgets.remove(lastIndex);
			}
			while (commentsWidgets.size() < comments.size()) {
				var newWidget = new ReviewLineWidget(cmpLines, SWT.NONE, userAccess, projectAccess, objectTypeAccess);
				newWidget.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
				commentsWidgets.add(newWidget);
			}
			for (int i = 0; i < comments.size(); i++) {
				var widget = commentsWidgets.get(i);
				widget.setReferenceAuthor(review.review.createdBy);
				widget.setReviewLine(review.destination, comments.get(i));
				widget.setEditHandler(this::onEditClicked);
				widget.setDeleteHandler(this::deleteEditingLine);
				widget.setAssignHandler(this::assignEditingLine);
				widget.setUnassignHandler(this::unassignEditingLine);
				widget.setCompleteHandler(this::completeEditingLine);
				widget.setUncompleteHandler(this::uncompleteEditingLine);
				widget.setEditable(true);
				widget.refresh();
			}

			wgtReviewers.setUsers(review.destination, reviewers);
			wgtAudience.setUsers(review.destination, audience);
		}
		composite.layout(true, true);
		updateScrollContentSize();
	}

	private void onEditClicked(ReviewLine line) {
		editMode = true;
		editingLineWidget.setReviewLine(review.destination, line);
		editingLineWidget.refresh();
		refresh();
	}

	@Inject
	void setSelection(@Optional @Named(IServiceConstants.ACTIVE_SELECTION) Object contact) {
		if (!ready) {
			return;
		}

		System.out.println("Selection: " + contact);
		if (contact != null) {
			System.out.println("Selection Class: " + contact.getClass());
		}

		var selectedObject = selectionAccess.getObjectFromSelection(contact);
		editingLineWidget.setSelectedObjectInEditor(selectedObject);
	}

}
