package net.founderio.adtcodereview.model;

public class ReviewLinesSetCompletedRequest {
	public boolean completed;

	public ReviewLinesSetCompletedRequest() {
		completed = false;
	}

	public ReviewLinesSetCompletedRequest(boolean completed) {
		this.completed = completed;
	}
}
