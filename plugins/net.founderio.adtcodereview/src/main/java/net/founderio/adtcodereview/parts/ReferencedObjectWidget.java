package net.founderio.adtcodereview.parts;

import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.Bullet;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.custom.LineStyleEvent;
import org.eclipse.swt.custom.LineStyleListener;
import org.eclipse.swt.custom.ST;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.GlyphMetrics;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import net.founderio.adtcodereview.AdtObjectTypeAccess;
import net.founderio.adtcodereview.AdtProjectAccess;
import net.founderio.adtcodereview.model.ReferencedObject;

public class ReferencedObjectWidget extends Composite {

	private Composite composite;

	private Button btnOpenObject;
	private CLabel lblObject;
	private Label lblObjectSeparator;
	private CLabel lblEnclosedObject;

	private ReferencedObject obj;
	private String destinationId;
	private StyledText txtSourceExtract;

	AdtProjectAccess projectAccess;
	AdtObjectTypeAccess objectTypeAccess;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public ReferencedObjectWidget(Composite parent, int style, AdtProjectAccess projectAccess,
			AdtObjectTypeAccess objectTypeAccess) {
		super(parent, style);
		this.projectAccess = projectAccess;
		this.objectTypeAccess = objectTypeAccess;

		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		setLayout(gridLayout);

		composite = new Composite(this, SWT.NONE);
		RowLayout rl_composite = new RowLayout(SWT.HORIZONTAL);
		rl_composite.marginBottom = 0;
		rl_composite.marginTop = 0;
		rl_composite.marginRight = 0;
		rl_composite.marginLeft = 0;
		rl_composite.center = true;
		composite.setLayout(rl_composite);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		btnOpenObject = new Button(composite, SWT.NONE);
		btnOpenObject.setToolTipText("Open Object");
		btnOpenObject.addSelectionListener(SelectionListener.widgetSelectedAdapter(this::onOpenObjectPressed));
		btnOpenObject.setImage(org.eclipse.wb.swt.ResourceManager.getPluginImage("net.founderio.adtcodereview",
				"icons/gotoobj_tsk.png"));

		lblObject = new CLabel(composite, SWT.NONE);
		lblObject.setText("Object");

		lblObjectSeparator = new Label(composite, SWT.NONE);
		lblObjectSeparator.setText(">");

		lblEnclosedObject = new CLabel(composite, SWT.NONE);
		lblEnclosedObject.setText("Enclosed Object");

		txtSourceExtract = new StyledText(this, SWT.BORDER);
		txtSourceExtract.setLeftMargin(2);
		txtSourceExtract.setEditable(false);
		txtSourceExtract.setFont(JFaceResources.getFont(JFaceResources.TEXT_FONT));
		txtSourceExtract.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		// Add row numbering
		txtSourceExtract.addLineStyleListener(new LineStyleListener() {
			@Override
			public void lineGetStyle(LineStyleEvent event) {
				// Set the line number
				int offset = 1;
				if (obj != null) {
					offset = (int) obj.relativeLine;
				}
				event.bulletIndex = txtSourceExtract.getLineAtOffset(event.lineOffset) + offset;

				// Set the style, 12 pixles wide for each digit
				StyleRange style = new StyleRange();
				style.metrics = new GlyphMetrics(0, 0,
						Integer.toString(txtSourceExtract.getLineCount() + offset).length() * 12);

				// Create and set the bullet
				event.bullet = new Bullet(ST.BULLET_NUMBER, style);
			}
		});
	}

	public void setObject(String destinationId, ReferencedObject obj) {

		this.destinationId = destinationId;
		this.obj = obj;
		if (obj == null || !obj.isSet()) {
			setVisible(false);

			lblObject.setText("");
			lblEnclosedObject.setText("");
			txtSourceExtract.setText("");

			lblObjectSeparator.setVisible(false);
			lblEnclosedObject.setVisible(false);
			return;
		}

		setVisible(true);

		if (obj.objectName == null) {
			lblObject.setText("");
			lblObject.setImage(null);
		} else {
			Image typeImage = objectTypeAccess.getTypeImage(obj.objectType);
			String typeName = objectTypeAccess.getFormattedTypeName(obj.objectType);
			String typeAndName = String.format("[%s] %s", typeName, obj.objectName);
			if (typeImage == null) {
				lblObject.setText(typeAndName);
			} else {
				lblObject.setText(obj.objectName);
			}
			lblObject.setToolTipText(typeAndName);
			lblObject.setImage(typeImage);
		}

		boolean hasEnclosedObject = obj.enclosedObjectName != null && !obj.enclosedObjectName.isEmpty();
		lblObjectSeparator.setVisible(hasEnclosedObject);
		lblEnclosedObject.setVisible(hasEnclosedObject);
		if (hasEnclosedObject) {

			Image typeImage = objectTypeAccess.getTypeImage(obj.enclosedObjectType);
			String typeName = objectTypeAccess.getFormattedTypeName(obj.enclosedObjectType);
			String typeAndName = String.format("[%s] %s", typeName, obj.enclosedObjectName);
			if (typeImage == null) {
				lblEnclosedObject.setText(typeAndName);
			} else {
				lblEnclosedObject.setText(obj.enclosedObjectName);
			}
			lblEnclosedObject.setToolTipText(typeAndName);
			lblEnclosedObject.setImage(typeImage);
		} else {
			lblEnclosedObject.setText("");
			lblEnclosedObject.setImage(null);
		}

		txtSourceExtract.setText(obj.sourceCodeExtract);

		layout(true, true);
	}

	public void onOpenObjectPressed(SelectionEvent e) {
		if (obj == null) {
			return;
		}
		projectAccess.openObject(destinationId, obj.adtUri);
	}
}
