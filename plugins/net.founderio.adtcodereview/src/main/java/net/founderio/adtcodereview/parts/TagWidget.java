package net.founderio.adtcodereview.parts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.ResourceManager;

import jakarta.annotation.Nonnull;

public class TagWidget extends Composite {
	private Runnable deleteHandler;
	private Label lblText;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public TagWidget(Composite parent, int style) {
		super(parent, SWT.BORDER);
		setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_FOREGROUND));
		setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
		RowLayout rowLayout = new RowLayout(SWT.HORIZONTAL);
		rowLayout.center = true;
		rowLayout.wrap = false;
		setLayout(rowLayout);

		lblText = new Label(this, SWT.NONE);
		lblText.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_FOREGROUND));
		lblText.setText("Text");

		Button btnNewButton = new Button(this, SWT.NONE);
		btnNewButton.setLayoutData(new RowData(15, 15));
		btnNewButton.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_LIST_BACKGROUND));
		btnNewButton.setToolTipText("Remove");
		btnNewButton.setImage(ResourceManager.getPluginImage("net.founderio.adtcodereview", "icons/close_view.png"));
		btnNewButton.addSelectionListener(SelectionListener.widgetSelectedAdapter(this::onDeletePressed));
		btnNewButton.addPaintListener(new PaintListener() {
			@Override
			public void paintControl(PaintEvent event) {
				event.gc.setBackground(TagWidget.this.getBackground());
				var size = btnNewButton.getSize();
				event.gc.fillRectangle(0, 0, size.x, size.y);
				event.gc.setBackground(btnNewButton.getBackground());
				event.gc.fillOval(0, 0, size.x, size.y);
				Image image = btnNewButton.getImage();
				var imageBounds = image.getBounds();
				event.gc.drawImage(image, (size.x - imageBounds.width) / 2, (size.y - imageBounds.height) / 2);
			}
		});
	}

	public void onDeletePressed(SelectionEvent e) {
		if (deleteHandler != null) {
			deleteHandler.run();
		}
	}

	public void setDeleteHandler(Runnable deleteHandler) {
		this.deleteHandler = deleteHandler;
	}

	public void setText(@Nonnull String text) {
		lblText.setText(text);
	}

}
