package net.founderio.adtcodereview;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.swt.graphics.Image;

import com.sap.adt.tools.core.ui.AbapCoreUi;

@SuppressWarnings("restriction")
@Creatable
public class AdtObjectTypeAccess {
	public String getTypeDisplayName(String workbenchObjectType) {
		if (workbenchObjectType == null || workbenchObjectType.isEmpty()) {
			return null;
		}
		var typeInfo = AbapCoreUi.getObjectTypeRegistry().getObjectTypeByGlobalWorkbenchType(workbenchObjectType);
		if (typeInfo == null) {
			return null;
		}
		return typeInfo.getDisplayName();
	}

	public String getFormattedTypeName(String workbenchObjectType) {
		if (workbenchObjectType == null) {
			return "";
		}
		var typeDisplayName = getTypeDisplayName(workbenchObjectType);
		if (typeDisplayName == null) {
			return workbenchObjectType;
		}
		return String.format("%s (%s)", typeDisplayName, workbenchObjectType);
	}

	public Image getTypeImage(String workbenchObjectType) {
		if (workbenchObjectType == null || workbenchObjectType.isEmpty()) {
			return null;
		}
		var typeInfo = AbapCoreUi.getObjectTypeRegistry().getObjectTypeByGlobalWorkbenchType(workbenchObjectType);
		if (typeInfo == null) {
			return null;
		}
		return typeInfo.getImage();
	}
}
