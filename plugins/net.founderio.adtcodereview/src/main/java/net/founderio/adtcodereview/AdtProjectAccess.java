package net.founderio.adtcodereview;

import java.util.Arrays;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.progress.IProgressService;

import com.sap.adt.destinations.logon.AdtLogonServiceFactory;
import com.sap.adt.destinations.ui.logon.AdtLogonServiceUIFactory;
import com.sap.adt.tools.core.AbapCore;
import com.sap.adt.tools.core.model.adtcore.IAdtCoreFactory;
import com.sap.adt.tools.core.model.adtcore.IAdtObjectReference;
import com.sap.adt.tools.core.project.AdtProjectServiceFactory;
import com.sap.adt.tools.core.project.IAbapProject;
import com.sap.adt.tools.core.system.IAbapSystemInfo;
import com.sap.adt.tools.core.ui.dialogs.AbapProjectSelectionDialog;
import com.sap.adt.tools.core.ui.navigation.AdtNavigationServiceFactory;
import com.sap.adt.tools.core.ui.navigation.IAdtNavigationService;

import jakarta.inject.Inject;

@SuppressWarnings("restriction")
@Creatable
public class AdtProjectAccess {
	@Inject
	private IProgressService progressService;

	@Inject
	private UISynchronize sync;
	@Inject
	private IWorkbench workbench;

	/**
	 * Get available ABAP projects in the workspace
	 * 
	 * @return
	 */
	public List<IProject> getAllABAPProjects() {
		return Arrays.asList(AdtProjectServiceFactory.createProjectService().getAvailableAbapProjects());
	}

	public IProject getProject(String destination) {
		return AdtProjectServiceFactory.createProjectService().findProject(destination);
	}

	public void ensureLoggedIn(IProject project) {
		var abapProject = project.getAdapter(IAbapProject.class);
		if (abapProject == null) {
			return;
		}

		ensureLoggedIn(abapProject);
	}

	public void ensureLoggedIn(IAbapProject abapProject) {
		sync.syncExec(() -> AdtLogonServiceUIFactory.createLogonServiceUI()
				.ensureLoggedOn(abapProject.getDestinationData(), progressService));
	}

	public boolean isLoggedIn(IProject project) {
		var abapProject = project.getAdapter(IAbapProject.class);
		if (abapProject == null) {
			return false;
		}

		return isLoggedIn(abapProject);
	}

	public boolean isLoggedIn(IAbapProject abapProject) {
		return AdtLogonServiceFactory.createLogonService().isLoggedOn(abapProject.getDestinationId());
	}

	public String getProjectDestination(IProject project) {
		var abapProject = project.getAdapter(IAbapProject.class);
		if (abapProject == null) {
			return null;
		}
		return abapProject.getDestinationId();
	}

	/**
	 * Asks the user to choose an ABAP Project. Talks to the UI thread.
	 * 
	 * @return An ABAP Project, or null if the user did not choose a project (e.g.
	 *         via cancel).
	 */
	public IProject chooseAbapProject() {
		/**
		 * Required wrapper due to effectively-final issue
		 */
		var wrapper = new Object() {
			IProject rv = null;
		};
		sync.syncExec(() -> {
			wrapper.rv = AbapProjectSelectionDialog.open(workbench.getActiveWorkbenchWindow().getShell(), null);
		});
		return wrapper.rv;
	}

	public IAbapSystemInfo getSystemInfo(String destination) {
		return AbapCore.getInstance().getAbapSystemInfo(destination);
	}

	public String getLoggedInUser(String destination) {
		var project = AdtProjectServiceFactory.createProjectService().findProject(destination);
		var abapProject = project.getAdapter(IAbapProject.class);
		if (abapProject == null) {
			return null;
		}
		return abapProject.getDestinationData().getUser();
	}

	public void openObject(String destination, String adtUri) {
		IAdtObjectReference ref = IAdtCoreFactory.eINSTANCE.createAdtObjectReference();
		ref.setUri(adtUri);
		var project = getProject(destination);
		IAdtNavigationService navigationService = AdtNavigationServiceFactory.createNavigationService();
		navigationService.navigate(project, ref, true);
	}
}
