package net.founderio.adtcodereview;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.sap.adt.communication.content.AdtMediaType;
import com.sap.adt.communication.content.ContentHandlerException;
import com.sap.adt.communication.content.IContentHandler;
import com.sap.adt.communication.message.ByteArrayMessageBody;
import com.sap.adt.communication.message.IMessageBody;

public class JacksonContentHandler<T> implements IContentHandler<T> {
	protected final ObjectMapper mapper;
	protected final Class<T> cls;

	public JacksonContentHandler(Class<T> cls) {
		mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		this.cls = cls;
	}

	@Override
	public T deserialize(IMessageBody arg0, Class<? extends T> arg1) {
		try {
			return mapper.readValue(arg0.getContent(), arg1);
		} catch (IOException e) {
			throw new ContentHandlerException("Error unmarshalling reviews entity", e);
		}
	}

	@Override
	public String getSupportedContentType() {
		return AdtMediaType.APPLICATION_JSON;
	}

	@Override
	public Class<T> getSupportedDataType() {
		return cls;
	}

	@Override
	public IMessageBody serialize(T arg0, Charset arg1) {
		try (var stream = new ByteArrayOutputStream();) {
			if (arg1 == null) {
				arg1 = StandardCharsets.UTF_8;
			}
			try (var sw = new OutputStreamWriter(stream, arg1)) {
				mapper.writeValue(sw, arg0);
			}

			return new ByteArrayMessageBody(getSupportedContentType(), stream.toByteArray());
		} catch (IOException e) {
			throw new ContentHandlerException("Error marshalling reviews entity", e);
		}
	}

}
