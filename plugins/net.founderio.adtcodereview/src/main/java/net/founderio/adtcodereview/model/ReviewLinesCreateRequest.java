package net.founderio.adtcodereview.model;

public class ReviewLinesCreateRequest {
	/**
	 * Unbounded string on backend. Meant to hold only one line of text for overview
	 * purposes.
	 */
	public String title;
	/**
	 * Unbounded string on backend. Multi-line longtext description.
	 */
	public String longText;
	public ReferencedObject referencedObject;
}
