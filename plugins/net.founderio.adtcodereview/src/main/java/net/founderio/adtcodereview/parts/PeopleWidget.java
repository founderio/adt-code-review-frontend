package net.founderio.adtcodereview.parts;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import jakarta.annotation.Nonnull;
import net.founderio.adtcodereview.AdtProjectAccess;
import net.founderio.adtcodereview.AdtUserAccess;

public class PeopleWidget extends Composite {
	AdtUserAccess userAccess;
	AdtProjectAccess projectAccess;

	private Consumer<String[]> addHandler;
	private Consumer<String> deleteHandler;
	private Button btnAdd;

	private String destination;
	private final List<String> users = new ArrayList<>();
	private final List<TagWidget> userWidgets = new ArrayList<>();

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public PeopleWidget(Composite parent, int style, AdtUserAccess userAccess, AdtProjectAccess projectAccess) {
		super(parent, style);
		this.userAccess = userAccess;
		this.projectAccess = projectAccess;
		setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_LIST_BACKGROUND));
		setLayout(new RowLayout(SWT.HORIZONTAL));

		btnAdd = new Button(this, SWT.FLAT);
		btnAdd.setLayoutData(new RowData(25, 25));
		btnAdd.setToolTipText("Add Users...");
		btnAdd.addSelectionListener(SelectionListener.widgetSelectedAdapter(this::onAddClicked));
		btnAdd.setImage(
				org.eclipse.wb.swt.ResourceManager.getPluginImage("net.founderio.adtcodereview", "icons/add_obj.png"));

		refresh();
	}

	public void setAddHandler(Consumer<String[]> addHandler) {
		this.addHandler = addHandler;
	}

	public void setDeleteHandler(Consumer<String> deleteHandler) {
		this.deleteHandler = deleteHandler;
	}

	public void setUsers(@Nonnull String destination, @Nonnull List<String> users) {
		this.destination = destination;
		this.users.clear();
		this.users.addAll(users);
		refresh();
	}

	public void onAddClicked(SelectionEvent e) {
		if (this.addHandler == null) {
			return;
		}
		String[] newUsers = userAccess.selectUserNamesFromDialog(getShell(), true,
				projectAccess.getProject(destination), null);
		if (newUsers != null && newUsers.length > 0) {
			this.addHandler.accept(newUsers);
		}
	}

	public void refresh() {
		while (userWidgets.size() > users.size()) {
			int lastIndex = userWidgets.size() - 1;
			userWidgets.get(lastIndex).dispose();
			userWidgets.remove(lastIndex);
		}
		while (userWidgets.size() < users.size()) {
			var newWidget = new TagWidget(this, SWT.NONE);
			userWidgets.add(newWidget);
			newWidget.moveAbove(btnAdd);
		}
		for (int i = 0; i < users.size(); i++) {
			var widget = userWidgets.get(i);
			var userName = users.get(i);
			var user = userAccess.getUser(null, destination, userName);
			if (user == null) {
				widget.setText(userName);
			} else {
				widget.setText(String.format("%s [%s]", user.text(), userName));
			}
			widget.setDeleteHandler(() -> {
				if (deleteHandler != null) {
					deleteHandler.accept(userName);
				}
			});
		}

		layout(true, true);
	}

}
