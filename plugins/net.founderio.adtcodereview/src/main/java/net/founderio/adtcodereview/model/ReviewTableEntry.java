package net.founderio.adtcodereview.model;

public class ReviewTableEntry {
	/**
	 * The ABAP Project destination ID from which this review was loaded.
	 */
	public String destination;
	public ReviewHeader review;

	public ReviewTableEntry(String destination, ReviewHeader review) {
		super();
		this.destination = destination;
		this.review = review;
	}
}