package net.founderio.adtcodereview.parts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.ICoreRunnable;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.EPartService.PartState;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchActionConstants;

import com.sap.adt.destinations.model.IDestinationData;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import net.founderio.adtcodereview.Activator;
import net.founderio.adtcodereview.AdtProjectAccess;
import net.founderio.adtcodereview.EventTopics;
import net.founderio.adtcodereview.ReviewsRepository;
import net.founderio.adtcodereview.model.ReviewHeader;
import net.founderio.adtcodereview.model.ReviewTableEntry;
import net.founderio.adtcodereview.model.ReviewsResponse;

public class ReviewsView {

	@Inject
	IWorkbench workbench;

//	@Inject
//	IWorkbenchPartSite site;

	@Inject
	EPartService partService;

	@Inject
	UISynchronize sync;

	@Inject
	ReviewsRepository repository;

	@Inject
	AdtProjectAccess projectAccess;

	private TableViewer viewer;
	private Action actionRefresh;
	private Action actionCreateReview;
	private Action doubleClickAction;

	private boolean ready;

	private List<ReviewTableEntry> reviews = new ArrayList<ReviewTableEntry>();

	@Inject
	public void adtLogonListenerHandler(
			@Optional @UIEventTopic(EventTopics.TOPIC_ADT_LOGON) IDestinationData destinationData) {
		refresh();
	}

	@PostConstruct
	public void createPartControl(Composite parent) {
		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);

		viewer.getTable().setHeaderVisible(true);

		viewer.setContentProvider(ArrayContentProvider.getInstance());
		viewer.setInput(new ReviewHeader[] {});

		var colTitle = new TableViewerColumn(viewer, SWT.NONE);
		colTitle.getColumn().setText("Title");
		colTitle.getColumn().setWidth(200);
		colTitle.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				var review = (ReviewTableEntry) element;
				return review.review.title;
			}
		});

		var colCreatedBy = new TableViewerColumn(viewer, SWT.NONE);
		colCreatedBy.getColumn().setText("Created By");
		colCreatedBy.getColumn().setWidth(120);
		colCreatedBy.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				var review = (ReviewTableEntry) element;
				return review.review.createdBy;
			}
		});

		var colID = new TableViewerColumn(viewer, SWT.NONE);
		colID.getColumn().setText("ID");
		colID.getColumn().setWidth(200);
		colID.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				var review = (ReviewTableEntry) element;
				return review.review.reviewId;
			}
		});

		var colCreatedOn = new TableViewerColumn(viewer, SWT.NONE);
		colCreatedOn.getColumn().setText("Created On");
		colCreatedOn.getColumn().setWidth(120);
		colCreatedOn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				var review = (ReviewTableEntry) element;
				if (review.review.createdOn == null) {
					return "";
				} else {
					return review.review.createdOn.toString();
				}
			}
		});

		var colProject = new TableViewerColumn(viewer, SWT.NONE);
		colProject.getColumn().setText("Project");
		colProject.getColumn().setWidth(200);
		colProject.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				var review = (ReviewTableEntry) element;
				return review.destination;
			}
		});

		ready = true;

		// Create the help context id for the viewer's control
		workbench.getHelpSystem().setHelp(viewer.getControl(), "net.founderio.adtcodereview.viewer");
//		site.setSelectionProvider(viewer);
		makeActions();
		hookContextMenu();

		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				ReviewsView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
//		site.registerContextMenu(menuMgr, viewer);
	}

	private void fillContextMenu(IMenuManager manager) {
		manager.add(actionRefresh);
		manager.add(actionCreateReview);
		// Other plug-ins can contribute their actions here
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void refresh() {
		Job.create("Refresh Reviews", (ICoreRunnable) monitor -> {
			var projects = projectAccess.getAllABAPProjects();
			monitor.beginTask("Loading reviews from all logged in ABAP projects", projects.size());

			this.reviews.clear();

			for (var iterator = projects.iterator(); iterator.hasNext();) {
				IProject project = iterator.next();
				if (!projectAccess.isLoggedIn(project)) {
					monitor.worked(1);
					continue;
				}

				String destinationID = projectAccess.getProjectDestination(project);

				ReviewsResponse response = repository.getReviews(destinationID);

				for (var iteratorResponse = response.reviews.iterator(); iteratorResponse.hasNext();) {
					ReviewHeader review = iteratorResponse.next();
					this.reviews.add(new ReviewTableEntry(destinationID, review));
				}

				monitor.worked(1);
			}

			sync.asyncExec(() -> {
				// ADT Login event is sometimes received before PostConstruct is done,
				// which means the UI elements will not be there yet.
				if (!ready) {
					return;
				}
				viewer.setInput(this.reviews);
			});

		}).schedule();

	}

	private void createReview() {

		Job.create("Create Review", (ICoreRunnable) monitor -> {
			var project = projectAccess.chooseAbapProject();
			if (project == null) {
				return;
			}

			String destinationId = projectAccess.getProjectDestination(project);
			var response = repository.createReview(destinationId, null);

			var newTableEntry = new ReviewTableEntry(destinationId, response.review);
			this.reviews.add(newTableEntry);
			refreshView();
			openReview(newTableEntry);
		}).schedule();
	}

	private void refreshView() {
		sync.asyncExec(() -> {
			// ADT Login event is sometimes received before PostConstruct is done,
			// which means the UI elements will not be there yet.
			if (!ready) {
				return;
			}
			viewer.setInput(reviews);
		});
	}

	private void openReview(ReviewTableEntry entry) {
		sync.asyncExec(() -> {
			MPart part = partService.showPart(ReviewCompanionView.PART_ID, PartState.ACTIVATE);
			var view = (ReviewCompanionView) part.getObject();
			view.setReview(entry);
		});
	}

	private void makeActions() {
		actionRefresh = new Action() {
			public void run() {
				refresh();
			}
		};
		actionRefresh.setText("Refresh");
		actionRefresh.setToolTipText("Refresh Reviews from Systems");
		actionRefresh.setImageDescriptor(
				ImageDescriptor.createFromURL(Activator.getContext().getBundle().getEntry("icons/refresh.png")));

		actionCreateReview = new Action() {
			public void run() {
				createReview();
			}
		};
		actionCreateReview.setText("Create new Review");
		actionCreateReview.setToolTipText("Create a new review (the destination system is chosen before creating it)");
		actionCreateReview
				.setImageDescriptor(workbench.getSharedImages().getImageDescriptor(ISharedImages.IMG_OBJ_ADD));

		doubleClickAction = new Action() {
			public void run() {
				IStructuredSelection selection = viewer.getStructuredSelection();
				Object obj = selection.getFirstElement();

				if (obj instanceof ReviewTableEntry entry) {
					openReview(entry);
				}
			}
		};
	}

	@Focus
	public void setFocus() {
		if (!ready) {
			return;
		}
		viewer.getControl().setFocus();
	}
}
