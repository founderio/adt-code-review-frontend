package net.founderio.adtcodereview;

import com.sap.adt.destinations.model.IDestinationData;

public interface EventTopics {
	/**
	 * ADT Logon event topic. Data object is of type {@link IDestinationData}
	 */
	public static final String TOPIC_ADT_LOGON = "net/founderio/adtcodereview/adt/logon";
}
