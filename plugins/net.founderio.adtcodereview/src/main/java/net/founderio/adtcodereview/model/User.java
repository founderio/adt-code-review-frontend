package net.founderio.adtcodereview.model;

public record User(String id, String text) {
}
