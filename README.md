# ADT Code Review



## Build and test everything

```sh
mvn clean verify
```

## Updating version numbers

```sh
mvn org.eclipse.tycho:tycho-versions-plugin:set-version -DnewVersion=0.1.0-SNAPSHOT -Dtycho.mode=maven
```
